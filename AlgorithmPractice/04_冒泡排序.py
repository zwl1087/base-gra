"""

0 ~ n-1：从第 0 个元素开始， 与下一个元素比较，谁大谁移动向右侧，直接完全遍历，第 n-1 位置元素就是 0 ~ n-1 中最大值
0 ~ n-2：从第 0 个元素开始， 与下一个元素比较，谁大谁移动向右侧，直接完全遍历，第 n-2 位置元素就是 0 ~ n-2 中最大值
0 ~ n-3：从第 0 个元素开始， 与下一个元素比较，谁大谁移动向右侧，直接完全遍历，第 n-3 位置元素就是 0 ~ n-3 中最大值
……
0 ~ n-i：从第 0 个元素开始， 与下一个元素比较，谁大谁移动向右侧，直接完全遍历，第 n-i 位置元素就是 0 ~ n-i 中最大值
……
0 ~ 1：从第 0 个元素开始， 与下一个元素比较，谁大谁移动向右侧，直接完全遍历，第 1 位置元素就是 0 ~ 1 中最大值


冒泡排序具有稳定性，元素相等时，不交换元素
"""


# 选择排序
def order_by_bubble_asc(array_list: list):
    if len(array_list) < 1:
        return

    # 在 0 - n-1 范围循环遍历
    for i in range(len(array_list)):
        # 假设 0 - n-1 范围的最后一个元素是最大值
        max_index = len(array_list) - i - 1
        # 在0 - i 范围寻找最大值： 逐个和最大值比较
        for j in range(len(array_list) - i):
            # 如果当前值比最大值大，则把当前值放在最后
            if array_list[j] > array_list[max_index]:
                array_list[max_index], array_list[j] = array_list[j], array_list[max_index]


# 冒泡排序
def order_by_bubble_desc(array_list: list):
    if len(array_list) < 2:
        return array_list
    # 在0 - n-1 上将最大值放在最后
    for i in range(len(array_list) - 1, -1, -1):
        # 在0 - i 上逐个遍历，不断的将最大值位置右移，直至最后一个元素是最大值
        for j in range(i):
            # 当前元素 比下一个元素大时，将当前元素与下一个元素交换位置
            if array_list[j + 1] < array_list[j]:
                # 交换元素的位置
                array_list[j + 1], array_list[j] = array_list[j], array_list[j + 1]
                print(array)


array = [1, 1, 2, 17, 1, 1, 2, 17, 0, 18, -2, 9, 0, 18, -2, 9]
order_by_bubble_asc(array)
print(array)

print("----------" * 10)
array = [1, 1, 2, 17, 0, 1, 1, 2, 17, 0, 18, -2, 9, 18, -2, 9]
order_by_bubble_desc(array)
print(array)
