# -*- coding: UTF-8 -*-
"""
@Project ：baseGra 
@File    ：2_求到达终点的所有路径_动态缓存.py
@Author  ：赵文龙 
@Date    ：2023/10/24 23:25 
@功能描述

"""
"""
分析： 
7->13(10)
    6->13(9)
        5->13(8)
        7->13(8)   -------|
                          | 7-13(8) 有重复解
    8->13(9)              |
        7->13(8)   -------|
        9->13(8)

动态规划的前提： 在暴力递归的过程中发现有重复解的时候，就可以用一直表记录重复解的值, 下一次求重复解时不用再去计算，直接从表中获取结果

解决问题： 从顶向下的动态规划，增加一个缓存，减少计算次数 【空间换时间】    --记忆化搜索


"""


def process2(cur, rest, aim, N, dp):
    """

    递归过程中， aim 和 N 是不变的， 只要cur 和 rest 确定，返回值也就确定了
    因此， cur 和 rest 是决定返回值的 key, 这个key是由两个变量组成的

    解决方案：  创建一个表，key 是有 cur和rest组成：  dp[cur][rest]

    cur的范围： 1-N
    rest的范围： 0-rest

    # dp的大小： [0-N][0-rest]:  可以容纳任意一个cur和rest

    """
    ans = 0
    # 如果不是第一次算的，直接返回
    if dp[cur][rest] != -1:
        return dp[cur][rest]

    # 如果是第一次算的，直接把返回值给 dp[cur][rest]
    if rest == 0:
        ans = 1 if cur == aim else 0
    elif cur == 1:
        ans = process2(2, rest - 1, aim, N, dp)
    elif cur == N:
        ans = process2(N - 1, rest - 1, aim, N, dp)
    else:
        ans = process2(cur - 1, rest - 1, aim, N, dp) + process2(cur + 1, rest - 1, aim, N, dp)

    dp[cur][rest] = ans

    return ans


def way2(cur, rest, aim, N):
    dp = [[-1 for j in range(rest + 1)] for i in range(N + 1)]
    print(dp)

    return process2(cur, rest, aim, N, dp)


if __name__ == '__main__':
    print(way2(2, 4, 4, 4))
