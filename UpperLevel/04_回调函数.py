def add(x, y):
    return x + y


def sub(x, y):
    if x > y:
        return x - y
    else:
        return y - x


"""
回调函数：将函数作为另一个函数的参数
"""


def calcu(x, y, callback):
    if str(x).isdigit() and str(y).isdigit():
        return callback(x, y)
    else:
        print(" 不是数字参与计算 ")
        return


print(calcu(1, 2, add))
print(calcu('a', 2, sub))
