"""
在一个数组中，
0 ~ n-1 范围内取一个最小值，然后将最小值与 0 位置上的元素交换，这样 0 ~ n-1 范围的最小值就在 0 位置上
1 ~ n-1 范围内取一个最小值，然后将最小值与 1 位置上的元素交换，这样 1 ~ n-1 范围的最小值就在 1 位置上
2 ~ n-1 范围内取一个最小值，然后将最小值与 2 位置上的元素交换，这样 2 ~ n-1 范围的最小值就在 2 位置上
……………………
n-2 ~ n-1 范围内取一个最小值，然后将最小值与 n-2 位置上的元素交换，这样 n-2 ~ n-1 范围的最小值就在 n-2 位置上

时间复杂度：
【常数操作分析】
0 ~ n-1 ：  查询 n 次，比较 n 次， 交换 1 次
1 ~ n-1 ：  查询 n-1 次，比较 n-1 次， 交换 1 次
2 ~ n-1 ：  查询 n-2 次，比较 n-2 次， 交换 1 次
……
n-2 ~ n-1 ：  查询 1 次，比较 1 次， 交换 1 次
【常数操作求和：】
query:    n + n-1 + n-2 + …… + 1
compare:  n + n-1 + n-2 + …… + 1
swap:     1 + 1 + 1 …… + 1

【时间复杂度】
total:  an^2 + bn + C

【大O表示法】： 一个算法的最大时间的估计
O(N^2)
根据操作操作的表达式，去除低阶项，保留高阶项，并且忽略高阶项的系数，最后保留下来的东西就是时间复杂度

平均值表示法
最小值表示法
【额外空间复杂度】 O(1)

"""


def order_by_select(arr: list) -> list:
    # 排除无效数据： 为空或者只有一个元素的数组
    if len(arr) <= 1:
        return arr
    # 实现从 i ~ n-1 逐个区间遍历查看： query
    for i in range(len(arr)):
        # 定义一个最小值的初始值
        min_index = i
        j = 0
        # 实现在 i ~ n-1 范围的比较： compare
        for j in range(i + 1, len(arr)):
            if arr[j] < arr[min_index]:
                # 如果当前值小于最小值，那么就任务最小值是当前值
                min_index = j
        # 当对比结果的最小值与 i ~ n-1 范围的第一个元素交换(arr[i])
        arr[min_index], arr[i] = arr[i], arr[min_index]


arr = [5, 33, 4, 62, 0, 15]
arr_0 = []
arr_1 = [1]
order_by_select(arr)
order_by_select(arr_1)
order_by_select(arr_0)
print(arr)
print(arr_0)
print(arr_1)
arr_2 = [1, 1, 2, 17, 0, 18, -2, 9]
order_by_select(arr_2)
print(arr_2)
