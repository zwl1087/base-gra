"""
求一个数组中的最大值

"""


def find_top(array: list, l, r):
    if l == r:
        return array[l]

    mid = l + ((r - l) >> 1)
    left_top = find_top(array, l, mid)
    right_top = find_top(array, mid + 1, r)
    return max(left_top, right_top)


a = [5, 4, 3, 6, 11]
print(find_top(a, 1, 4))
print(a)
