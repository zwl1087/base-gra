# -*- coding: utf-8 -*-
"""
@Time ： 2022/12/14 1:29
@Auth ： zhaowenlong
@File ：proc_demo.py
@Software: PyCharm

进程的使用
1、 创建进程
    子进程是由父进程创建的，创建必须依赖于父进程
2、 执行进程
    子进程的执行不依赖与主进程的存活
3、 启动进程
4、 进程访问全局变量： 每个进程独享全局变量，互不干扰

"""
from multiprocessing import Process
import time
import os
from ProcessAndThread import localtime


def task_a(n):
    while True:
        time.sleep(n)
        print(
            f"[ task_a.__name__ ]: {task_a.__name__}, [执行时间]: {localtime()}, [进程]: {os.getpid()}, [父进程]: {os.getppid()}")


def task_b(s):
    while True:
        time.sleep(s)
        print(
            f"[ task_b.__name__ ]: {task_b.__name__}, [执行时间]: {localtime()}, [进程]: {os.getpid()}, [父进程]: {os.getppid()}")


def start():
    pa = Process(target=task_a, name=" demo_1", args=(2,))
    print(pa.name)
    """
    target=task_a,   进程执行的任务 
    name=" demo_1",  进程的名称
    args=(2,)        进程执行函数的参数传递
    """
    pb = Process(target=task_b, name="demo_2", args=(3,))
    print(pb.name)
    pa.start()
    pb.start()


if __name__ == '__main__':
    start()
