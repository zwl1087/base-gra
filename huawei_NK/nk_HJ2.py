# -*- coding: utf-8 -*-
"""
@Time ： 2023/7/12 23:57
@Auth ： zhaowenlong
@File ：nk_HJ2.py
@Software: PyCharm
"""

line = ''
i = 0
while i < 2:
    line_test = input().lower()
    line += line_test + '\n'
    i += 1

test_str = line.split('\n')[0]
test_char = line.split('\n')[1]

count = 0
for item in list(map(str, test_str)):
    if item == test_char:
        count += 1

print(count)