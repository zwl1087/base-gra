# -*- coding: utf-8 -*-
"""
@Time ： 2022/12/15 1:03
@Auth ： zhaowenlong
@File ：proc_own_define.py
@Software: PyCharm

自定义进程

"""
import os
import time
from multiprocessing import Process
from ProcessAndThread import localtime


class MyProcess(Process):

    def __init__(self, name, func):
        self.func = func
        super().__init__(target=self.func)
        self.name = name

    def run(self) -> None:
        """
        每个自定义的进程 必须重写父类的run() 方法，完成自定义进程
        :return:
        """
        num = 1
        while True:
            print(f"第 {num} 执行 {self.name} ---> 自定义进程 \n")
            self.func(self.name)
            if num == 30:
                break
            num += 1


def task(name):
    time.sleep(3)
    print(f"{name} [pid:{os.getpid()}][ppid{os.getppid()}]: -->{ localtime()} \n")


if __name__ == '__main__':
    print("主进程已经开始了")
    p = MyProcess("zhaowenlong", task)
    # 调用start() 方法时，会自动去找run()方法
    p.start()
    s = MyProcess("xiaonaofu", task)
    s.start()
    print(" 主进程已经结束了 ")
