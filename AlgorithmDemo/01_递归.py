import os

"""
遍历某个文件夹，输出所有的文件

"""


def out_print_file_name(file_path):
    # 递归的出口： 如果是文件，则输出文文件名称
    if os.path.isfile(file_path):
        print(file_path)
        return file_path

    # 如果是目录，则继续进入目录判断
    for name in os.listdir(file_path):
        # 拼接绝对路径
        chd_path = os.path.join(file_path, name)
        # 如果是目录则继续递归判断
        out_print_file_name(chd_path)


if __name__ == '__main__':
    # print(os.path.isdir("E:\证件资料"))
    # print(os.path.isfile("E:\证件资料\结婚证照片\BC0R1956.JPG"))
    # print(os.listdir("E:\证件资料\结婚证照片"))
    # print(os.chdir("E:\证件资料\结婚证照片"))
    # print(os.path.join("1/", "2"))
    # out_print_file_name("E:\常用安装包")
    out_print_file_name("E:\证件资料")
    # s = [b for b in range(5) ]
