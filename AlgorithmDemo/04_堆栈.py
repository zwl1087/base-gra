from pythonds.basic.stack import Stack


class ArrayStack:
    """
    顺序栈： 空间是固定的

    """
    def __init__(self, capacity):
        # 定义一个数组，作为初始栈
        self.n = capacity
        self.data = [-1] * capacity
        self.count = 0

    def push(self, value):
        # 判断栈是否已满
        if self.count == self.n:
            return False
        # 如果未满：则将 value 入栈： 从最上方开始入栈，即从self.count 位置入栈
        self.data[self.count] = value
        # 入栈完成，移动指针： ps 指针是指向最后一个元素的下一位
        self.count += 1
        return True

    def pop(self):
        # 判断是否是空栈，如果是返回null
        if self.count == 0:
            return None
        # 如果有元素，则开始出栈
        # 从上开始出栈， 首先移动指针到第一个元素
        self.count -= 1
        # 将元素出栈并赋值
        value = self.data[self.count]
        # 返回出栈元素
        return value


class LinkStack:
    """
    链式栈： 空间是无限的
    定义：  节点：节点属性，节点指向
            栈： 定义头部位置
    """
    def __init__(self):
        # 定义head的位置，默认为None, head永远指向最后一个入栈的节点
        self.top = None

    def push(self, value):
        # 初始化一个新的节点
        new_node = self.Node(value)
        # 判断是否是空栈
        if self.top is None:
            # 空栈将节点直接入栈
            self.top = new_node
        else:
            # 链式栈非空时： 添加指向，新节点指向最后一个入栈的节点
            new_node.next = self.top
            # 将 head 指针挪动到入栈后的最后一个节点位置
            self.top = new_node

    def pop(self):
        # 判断是否时空栈，空栈返回null
        if self.top is None:
            return None
        # 执行出栈：将最后一个入栈的节点出栈，赋值给变量
        old_node = self.top.value
        # 移动指针： 指针挪动到出栈后的最后一个节点位置，也即出栈前指针指向的next
        self.top = self.top.next
        return old_node

    class Node:
        def __init__(self, value):
            # 节点保存的值
            self.value = value
            # 节点的指向
            self.next = None


if __name__ == '__main__':
    arr_s = ArrayStack(10)
    arr_s.push(2)
    arr_s.push(5)
    arr_s.push(4)
    print(arr_s.pop())
    print(arr_s.pop())
    print(arr_s.pop())
    print(arr_s.pop())
    lin_s = LinkStack()
    lin_s.push(3)
    lin_s.push(31)
    lin_s.push(32)
    lin_s.push(33)
    lin_s.push(34)
    lin_s.push(35)
    print(lin_s.pop())
    print(lin_s.pop())
    print(lin_s.pop())
    print(lin_s.pop())
    print(lin_s.pop())
    print(lin_s.pop())
    print(lin_s.pop())
    s = Stack()
    print(s.size())
    for i in "helloworld":
        s.push(i)

    print(s.peek())
    print(s.size())
    while not s.isEmpty():
        print(s.pop())



