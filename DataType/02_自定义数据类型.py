# -*- coding: utf-8 -*-
"""
@Time ： 2022/12/16 22:51
@Auth ： zhaowenlong
@File ：02_自定义数据类型.py
@Software: PyCharm
"""
from typing import List, Dict


class DemoPo:

    def __init__(self, code: int, msg: str, data: List[Dict]):
        self.code = code
        self.msg = msg
        self.data = data

    def __repr__(self):
        return self.__dict__


if __name__ == '__main__':
    dp = DemoPo(0, "success", data=[])
    print(dp.__dict__)

