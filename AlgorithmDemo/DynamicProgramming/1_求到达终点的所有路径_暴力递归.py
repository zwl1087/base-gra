# -*- coding: UTF-8 -*-
"""
@Project ：baseGra 
@File    ：1_求到达终点的所有路径_暴力递归.py
@Author  ：赵文龙 
@Date    ：2023/10/24 23:03 
@功能描述

"""
"""
有 1-N N个位置, 
当前位置 cur 目标位置 aim
从cur到aim 共可以走 rest 步
当到达 1 位置时，只能向右走
当到达 N 位置时，只能向左走

问： 共有多少种走法


思路：
    如果在1位置： 1 -> aim(rest)  等价于 2 -> aim(rest-1)
    如果在N位置： N -> aim(rest)  等价于 N-1 -> aim(rest-1)
    中间位置：  cur -> aim(rest)  等价于 cur-1 -> aim(rest-1) + cur+1 -> aim(rest-1)

"""


def process(cur, rest, aim, N):
    # 剩余步数为 0， 如果到达了aim 就+1 否则就是0
    if rest == 0:
        return 1 if cur == aim else 0

    if cur == 1:
        return process(2, rest - 1, aim, N)
    if cur == N:
        return process(N - 1, rest - 1, aim, N)

    return process(cur - 1, rest - 1, aim, N) + process(cur + 1, rest - 1, aim, N)


if __name__ == '__main__':
    print(process(2, 4, 4, 7))
