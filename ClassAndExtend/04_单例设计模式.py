"""
    实现单例设计模式：
            【python 的语言特点】
            1、类对象只有一个，即不管实例化多少次，类对象的内存地址都只有一个
            2、类对象的类属性、类方法 会被多个的实例对象所共享
    __new__：Object类提供的静态方法，有两个作用：1、在内存中为对象分配内存空间；2、返回对象的引用
    __init__：对象初始化
"""
import socket
from threading import Lock


class MusicPlayer:
    """
    实现饿汉模式的 单例模式

    """
    instance = None
    init_flag = False

    # 重写 __new__ 方法，并且返回对象的引用，实现单例设计
    def __new__(cls, *args, **kwargs):
        if not cls.instance:
            # 调用父类的__new__方法，并且接收一个类名参数， cls代表当前类
            cls.instance = super().__new__(cls)
        # 必须返回一个实例，否则就不会实例化
        return cls.instance

    # 实现类的初始化只进行一次：使用类属性做初始化的判断
    def __init__(self):
        if MusicPlayer.init_flag:
            print("已经初始化完成了，不需要再次执行了")
            return
        print("开始执行初始化动作了...")

        MusicPlayer.init_flag = True


class IdMaker(object):
    """
    实现懒汉模式的 单例模式
    """
    # 添加一个线程锁，避免多个实例间的并发实例化
    __instance_lock = Lock()
    __instance = None
    __id = -1

    def __new__(cls):
        # 在实例化的时候，没有创建实例对象，并且抛出异常
        raise ImportError("Instantition not allowed")

    @classmethod
    def get_instance(cls):
        # with : 将with的代码块自动加锁，代码执行完毕后，自动解锁
        with cls.__instance_lock:
            if cls.__instance is None:
                cls.__instance = super().__new__(cls)
        return cls.__instance

    def get_id(self):
        self.__id += 1
        return self.__id


if __name__ == '__main__':
    m1 = MusicPlayer()
    m2 = MusicPlayer()
    print(m1)
    print(m2)
    # import socket

    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(("8.8.8.8", 80))
    print(s.getsockname()[0])

    print("===" * 25)
    try:
        id_0 = IdMaker()
    except ImportError as e:
        print(e)
    finally:
        print("==="*25)
    id_1 = IdMaker.get_instance().get_id()
    id_2 = IdMaker.get_instance().get_id()
    id_3 = IdMaker.get_instance().get_id()
    print(id_1, id_2, id_3)
