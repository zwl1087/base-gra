"""
# 二叉搜索树: 二叉搜索树又称二叉排序树，具有以下性质,

    1、若它的左子树不为空，则左子树上所有节点的值都小于根节点的值
    2、若它的右子树不为空，则右子树上所有节点的值都大于根节点的值
    3、它的左右子树也分别为二叉搜索树

注意：二叉搜索树中序遍历的结果是有序的
先序：头左右
中序：左头右
后续：左右头
"""
import math

from pythonds.basic.stack import Stack
from queue import Queue
from AlgorithmDemo.BinaryTree import create_binary_tree, Nodes, bst


class BinarySearchTree:
    """

    搜索二叉树：


    """

    def __init__(self):
        self.tree = None

    class Node:
        """
            定义二叉树节点
        """

        def __init__(self, data):
            self.data = data
            self.left = None
            self.right = None

    def insert(self, value):
        # 如果根节点为空, 则将节点插入根节点
        if self.tree is None:
            self.tree = self.Node(value)
            return

        # 根节点不为空
        p = self.tree
        while p is not None:
            # 当前节点比根节点大, 则插入右子树
            if value > p.data:
                # 根节点没有右子树时，直接插入右孩子
                if p.right is None:
                    p.right = self.Node(value)
                    return
                # 根节点有右孩子, 将根节点移动到右孩子上继续判断
                p = p.right

            # 当前节点比根节点小, 则插入左子树
            elif value < p.data:
                # 根节点没有左子树时，直接插入左孩子
                if p.left is None:
                    p.left = self.Node(value)
                    return
                # 根节点有右孩子, 将根节点移动到右孩子上继续判断
                p = p.left

    def find(self, value):
        p = self.tree
        while p is not None:
            if value > p.data:
                p = p.right
            elif value < p.data:
                p = p.left
            else:
                return p
        return None

    def delete(self, value):
        p = self.tree
        p_parent = None
        # 寻找值为 value 的节点
        while p is not None and p.data != value:
            p_parent = p
            if value > p.data:
                p = p.right
            elif value < p.data:
                p = p.left

        # 如果节点没有找到时，直接返回
        if p is None:
            return
        if p.left is not None and p.right is not None:
            tmp_p = p.right
            tmp_parent = p
            # 在删除节点的右子树上找出 值最小的节点： 【这个节点必然是只有一个右孩子或者没有孩子的节点】
            while tmp_p.left is not None:
                tmp_parent = tmp_p
                tmp_p = p.left
            # 将最小值节点的值赋给删除节点
            p.data = tmp_p.data
            # 将最小值节点删除： 【技巧： 1、将最小值节点看作一个是一个待删除点，2、最小值节点必然只有一个子节点】
            p = tmp_p
            p_parent = tmp_parent

        # 待删除节点只有一个孩子,或者没有孩子时
        if p.left is not None:
            p_child = p.left
        elif p.right is not None:
            p_child = p.right
        else:
            p_child = None

        # 单个孩子或者没有孩子时的删除操作
        # 如果删除节点时根节点
        if p_parent is None:
            self.tree = p_child
        # 如果 删除节点是其父节点的左孩子： 让父节点的左孩子指向删除节点的孩子
        elif p_parent.left == p:
            p_parent.left = p_child
        # 如果删除节点是其父节点的右孩子：让父节点右孩子指向删除节点孩子
        elif p_parent.right == p:
            p_parent.right = p_child


class BinaryTree:
    def __init__(self):
        self.root = None

    class Node:
        def __init__(self, data):
            self.data = data
            self.left = None
            self.right = None

    def insert(self, value):
        new_node = self.Node(value)

        # 如果根节点没有值，插入根节点
        if self.root is None:
            self.root = new_node

        # 如果根节点不为空，则先左后右
        p = self.root
        while p is not None:

            if p.left is None:
                p.left = new_node
                return
            elif p.left is not None and p.right is None:
                p.right = new_node
                return
            elif p.left is not None and p.right is not None:
                p = p.left


def binary_tree_recursion_order(head: Nodes):
    """
        二叉树的递归序
    :param head: 二叉树的头节点
    :return:
    """

    if head is None:
        return

    # 二叉树先序遍历: 0 1 3 4 2 5 6
    print(head.data, end=' ')

    binary_tree_recursion_order(head.left)
    # 二叉树中序遍历： 3 1 4 0 5 2 6
    # print(head.data, end=' ')
    binary_tree_recursion_order(head.right)

    # 二叉树后续遍历:  3 4 1 5 6 2 0
    # print(head.data, end=' ')


# 先序遍历
def binary_tre_preorder_traversal(head: Nodes):
    if head is None:
        return
    print(head.data, end=' ')

    if head.left:
        binary_tre_preorder_traversal(head.left)

    if head.right:
        binary_tre_preorder_traversal(head.right)


def binary_tre_inorder_traversal(head: Nodes):
    if not head:
        return

    if head.left:
        binary_tre_inorder_traversal(head.left)

    print(head.data, end=' ')

    if head.right:
        binary_tre_inorder_traversal(head.right)


def binary_tree_postorder_traversal(head: Nodes):
    if not head:
        return
    if head.left:
        binary_tree_postorder_traversal(head.left)

    if head.right:
        binary_tree_postorder_traversal(head.right)

    print(head.data, end=' ')


#  二叉树非递归遍历
def binary_tree_ergodic(head: Nodes):
    """

        1、将根节点放入栈内
        2、如果栈不为空，则出栈， 出栈即打印当前节点的data
        3、同时，如果右孩子不为空，将右孩子放入栈中
        4、同时，如果左孩子不为空，将左孩子放入栈中
        5、如果左孩子、或右孩子为空，则PASS

    :param head:
    :return:
    """

    stack = Stack()
    print("二叉树非递归先序遍历结果： ")
    if head is not None:
        stack.push(head)
        while not stack.isEmpty():
            head = stack.pop()
            print(head.data, end=' ')
            if head.right is not None:
                stack.push(head.right)
            if head.left is not None:
                stack.push(head.left)
    print()


# 中序遍历
def inorder_traversal_unrecur(head: Nodes):
    print("二叉树非递归中序遍历结果： ")
    stack = Stack()
    if head is not None:  # 根节点不为空

        # 栈不为空：  当前处理节点
        while not stack.isEmpty() or head is not None:
            if head is not None:  # 将根节点 所有左孩子全部放入栈中
                stack.push(head)
                head = head.left
            else:
                head = stack.pop()  # 从最后一个左孩子开始， 弹出当前节点，并且打印当前节点的
                print(head.data, end=' ')
                head = head.right  # 将右孩子的所有左孩子放入栈中
    print()


# 后序遍历
def postorder_traversal_unrecur(head: Nodes):
    """
    初始化： 准备一个栈和一个辅助栈
    流程：
        1、如果根节点不为空, 将根节点放入栈,
        2、开始循环：如果栈不为空，将根节点出栈，出栈的同时放入辅助栈
        3、左孩子不为空，将左孩子放入栈，右孩子不为空，将右孩子放入栈
        4、继续出栈，将上一次的右孩子出栈, 出栈并放入辅助栈, 同时继续将右孩子的左右孩子放入栈, 如果左孩子、或右孩子为空，则PASS
        5、继续出栈，将上一次的右孩子出栈, 出栈并放入辅助栈, 同时继续将右孩子的左右孩子放入栈, 如果左孩子、或右孩子为空，则PASS
        6、将所有节点都放入辅助栈后, 循环结束
        7、辅助栈中的元素依次出栈，得到二叉树的后续遍历结果

    :param head:
    :return:
    """
    print("二叉树非递归后序遍历结果： ")
    stack = Stack()
    col_stack = Stack()
    if head is not None:
        stack.push(head)
        while not stack.isEmpty():
            head = stack.pop()
            col_stack.push(head)
            if head.left is not None:
                stack.push(head.left)
            if head.right is not None:
                stack.push(head.right)

    while not col_stack.isEmpty():
        cur = col_stack.pop()
        print(cur.data, end=' ')

    print()


# 二叉树宽度优先遍历
def breach_first_traversal(head: Nodes):
    print("二叉树宽度有限遍历结果： ")
    q = Queue()
    if head is not None:
        q.put(head)
        while not q.empty():
            head = q.get()
            print(head.data, end='')
            if head.left is not None:
                q.put(head.left)
            if head.right is not None:
                q.put(head.right)
    print()


# 求二叉树的最大宽度： map
def breach_first_traversal_max(head: Nodes):
    if head is None:
        print("当前二叉树是空树")
        return
    q = Queue()
    q.put(head)
    cur_level = 1
    max_nodes = -1
    cur_nodes_num = 0
    node_level_map = {head: cur_level}

    while not q.empty():
        cur = q.get()
        cur_node_level = node_level_map.get(cur)
        if cur_node_level == cur_level:
            cur_nodes_num += 1

        else:
            max_nodes = max(max_nodes, cur_nodes_num)
            cur_level += 1
            cur_nodes_num = 1

        if cur.left is not None:
            node_level_map[cur.left] = cur_level + 1
            q.put(cur.left)
        if cur.right is not None:
            node_level_map[cur.right] = cur_level + 1
            q.put(cur.right)
    max_nodes = max(max_nodes, cur_nodes_num)
    print("max_nodes", max_nodes)
    return max_nodes


# 求二叉树的最大宽度:  有限变量
def breach_first_traversal_max_queue(head: Nodes):
    q = Queue()
    q.put(head)
    max_num = -1
    cur_end = head  # 当前层的最后一个， 初始化时, 将根节点认为是当前层的最后一个
    next_end = None  # 下一层的最后， 即遍历时将当前的最后一个入队的孩子记为下一层的最后一个
    cur_node_num = 0

    while not q.empty():
        # 出队
        cur = q.get()

        # 分别将左、右孩子入队，并且记录为下一层的最后一个
        if cur.left is not None:
            q.put(cur.left)
            next_end = cur_end.left
        if cur.right is not None:
            q.put(cur.right)
            next_end = cur.right

        #  如果当前节点的是当前层的最后一个
        if cur_end == cur:
            # 节点数加 1
            cur_node_num += 1
            # 并且获取最大值
            max_num = max(max_num, cur_node_num)
            # 将当前层最后一个节点，更新成下一层的最后一个：  也就是当前层已经没有节点了，开始统计下一层的节点
            cur_end = next_end

            # 原先的下一层清空，开始记录新的一层
            next_end = None
            cur_node_num = 0
        else:
            # 如果当前节点不是当前层的最后一个，那么当前层的节点数直接加 1
            cur_node_num += 1
    return max_num


if __name__ == '__main__':
    root = create_binary_tree()

    print('------' * 10)
    binary_tree_ergodic(root)
    print("二叉树递归 先序遍历结果： ")
    binary_tre_preorder_traversal(root)
    print()
    print('------'*10)
    postorder_traversal_unrecur(root)
    print("二叉树递归 后序遍历结果： ")
    binary_tre_preorder_traversal(root)
    print()
    print('------'*10)
    inorder_traversal_unrecur(root)
    print("二叉树递归 中序遍历结果： ")
    binary_tre_inorder_traversal(root)
    print()
    print('------'*10)
    breach_first_traversal(root)
    print(breach_first_traversal_max(root))
    print(breach_first_traversal_max_queue(root))

    head = bst()
    inorder_traversal_unrecur(head)
