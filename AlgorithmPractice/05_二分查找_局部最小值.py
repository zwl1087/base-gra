"""
1、在一个有序的数组中，查询一个数{num}是否存在
    a、 找到中点的位置，判断 Mid 和 num 的大小关系，
    b、如果 num > mid，则继续查询 mid ~ n之间的数, 如果 num < mid, 则继续查询 0 ~ mid 之间的数据
    c、以此类推，直至找到 或者 数组全部查询结束 为止

时间复杂度： O(log2N) N表示数组的长度

2、在一个有序数组中，查找 >= x 的最左位置的数据：
3、在一个有序数组中，查找 >= x 的最右位置的数据：
【将数组二分并查找，直至满足条件的范围的数据都被遍历】
[1,1,1,2,2,2,2,2,2,3,3,3,3,4,4,4,4,4,4,5,5,5,5,5,5,5,6,6,6,6,7]


4、局部最小值 [ 局部极值 ]： arr数组中，相邻的两个数一定不相等
    {
        [0] < [1]
        [n-1] < [n-2]
        [m-1] > [m] && [m+1] > [m]
    }
    判断[0] 是不是局部最小
    判断[n-1] 是不是局部最小
    开始二分： 取中点M , 如果M > M-1,则在 [0,M] 上继续二分，直至找出局部最小点
    如果 M < M-1, 则在[M, N-1] 上继续二分，直至找出局部最小点


"""
import math
from random import random


def find_local_min_num(array: list):
    # arr为空
    if array is None or len(array) == 0:
        return -1
    # arr只有一个数
    if len(array) == 1:
        return 0
    # 第一个数据局部最小
    if array[0] < array[1]:
        return 0
    # 最后一个数是局部最小
    if arr[len(array) - 1] < array[len(array) - 2]:
        return len(array) - 1

    # 二分查找局部最小值
    l = 0
    r = len(array) - 1
    ans_index = -1
    while l <= r:
        mid = int((l + r) / 2)
        # 如果中点是局部最小
        if array[mid] < array[mid + 1] and array[mid] < array[mid - 1]:
            ans_index = mid
            break

        # 如果中点比他前一个数大，则必然在 0 ~ mid-1 上存在一个局部最小
        if array[mid] > array[mid - 1]:
            r = mid
            continue

        # 如果中点比他后一个数小，则必然在 mid+1 ~ n-1 上存在一个局部最小
        if array[mid] > array[mid + 1]:
            l = mid + 1
            continue

    return ans_index


def random_adjoin_diff_array(max_len: int, max_value: int, is_sorted: bool = False):
    """
        生成一个相邻的两个数一定不相等随机数组
    :param max_len:
    :param max_value:
    :param is_sorted:
    :return:
    """
    random_arr = [0] * math.floor(random() * max_len)

    if random_arr:
        random_arr[0] = math.floor(random() * max_value)
        # i = 1
        for i in range(1, len(random_arr)):
            while True:
                random_arr[i] = math.floor(random() * max_value)
                if random_arr[i] != random_arr[i - 1]:
                    break

        if is_sorted:
            random_arr.sort()
    return random_arr


def check(arr: list, index):
    if arr is None or len(arr) == 0:
        return True if index == -1 else False
    l = index - 1
    r = index + 1
    # print(f"len(arr): {len(arr)}, r= {r}, l= {l}, index= {index}")
    l_bigger = arr[l] > arr[index] if l >= 0 else True
    # print(f"l_bigger: {l_bigger}")
    r_bigger = arr[r] > arr[index] if r < len(arr) else True
    # print(f"r_bigger: {r_bigger}")

    return l_bigger and r_bigger


if __name__ == '__main__':

    print(" test start ")
    check_times = 100000
    for i in range(check_times):
        arr = random_adjoin_diff_array(100, 200)

        try:
            index = find_local_min_num(arr)
        except Exception as e:
            print(i, arr)
            break
        if not check(arr, find_local_min_num(arr)):
            print(arr)
            print(f"index: {index}, arr[index]: {arr[index]}")
            print(find_local_min_num(arr))
            print(check(arr, find_local_min_num(arr)))

    print(" test end")
