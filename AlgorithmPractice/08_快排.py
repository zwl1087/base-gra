# encoding=utf8
"""
【荷兰国旗问题】
给一个数组和一个数num,请把数组内小于等于num的数放在左边，大于num的数放在右边，要求时间复杂度O(1),空间复杂度O(N)
给一个数组和一个数num,请把数组内小于num的数放在左边,等于num的数放在中间,大于num的数放在右边，要求时间复杂度O(1),空间复杂度O(N)
"""
from math import floor
from random import random


def quick_order_1(array: list, num: int) -> list:
    """
    1.0版本：
        设定一个小于num的区间A，使得区间A的右边界和数组的左边界重合
        遍历数据并且比较大小：
            如果当前数小于等于num,则区域A的右边界数和当前数交换，A的右边界移动，同时遍历下一个；
            如果当前数大于num,则区域A不变，继续遍历下一个
            数组没有元素时遍历结束，此时得到的数据即是满足条件的数据
    :param array:
    :param num:
    :return:
    """
    if len(array) < 2:
        return array
    i = 0
    j = i - 1
    while i < len(array):
        if array[i] <= num:
            # 如果当前数小于num, 当前数和A的右边界交换
            array[j + 1], array[i] = array[i], array[j + 1]
            # 右边界移动
            j += 1
            # 继续遍历下一个
            i += 1
        else:
            # 如果当前数大于num, 继续遍历下一个
            i += 1
    return array


def quick_order_2(array: list, num: int) -> list:
    if len(array) < 2:
        return array
    l = 0
    r = len(array) - 1
    i = 0
    while i <= r:
        if array[i] < num:
            array[l], array[i] = array[i], array[l]
            l += 1
            i += 1
        elif array[i] == num:
            i += 1
        else:
            array[r], array[i] = array[i], array[r]
            r -= 1

    return [l, r]


def partition(array: list, l, r) -> list:
    # 假定小于区间是从l开始，并且向右移动
    less = l
    # 假定大于区间是从r-1开始，并且向左移动， array[r] 用作比较最后处理
    more = r - 1
    # less、more 也等同于定义等于区间的边界

    while l <= more:
        if array[l] < array[r]:
            array[less], array[l] = array[l], array[less]
            less += 1
            l += 1
        elif array[l] == array[r]:
            l += 1
        else:
            array[more], array[l] = array[l], array[more]
            more -= 1
    # 处理l 到 r-1 的元素处理完成后， 将 array[r] 放在等于区间的最后
    array[more + 1], array[r] = array[r], array[more + 1]
    # 返回等于区间的位置，
    return [less, more + 1]


# 【时间复杂度】： O(N*logN);【空间复杂度】： O(logN)
def quick_order_3(array: list, l, r):
    """
    快速排序的思想：
        1、用数组的最后的元素将数组划分成 >array[r]; = array[r]; >array[r] 三个部分，并且返回等于区间的边界P
        2、在 [l, p[0]-1]、和 [p[1]+1, r] 分别递归
    :param array:
    :param l:
    :param r:
    :return:
    """
    if l < r:
        l1 = l * floor(random() * (r - l + 1))
        print(array[l1])
        p = partition(array, l1, r)
        quick_order_3(array, l, p[0] - 1)
        quick_order_3(array, p[1] + 1, r)


if __name__ == '__main__':
    print("----" * 10, "quick_order_1 (荷兰国旗)")
    a = [3, 4, 5, 6, 9, 2, 5, 8]
    print(f"数组为：{a}")
    quick_order_1(a, 5)
    print(f"按照5将数组分类后的结果： {a}")
    print("----" * 10, "quick_order_2  (荷兰国旗)")
    a = [3, 4, 5, 8, 6, 8, 9, 2, 5, 8]
    print(f"数组为：{a}")
    print(f"返回的等于区间：{quick_order_2(a, 8)}")
    print(f"按照5将数组分类后的结果： {a}")
    print("----" * 10, "partition  (快速排序)")
    a = [3, 4, 8, 5, 6, 9, 11, 2, 5, 8]
    print(f"数组为：{a}")
    print(partition(a, 0, len(a) - 1))
    print(f"按照 array[r] 将数组分类后的结果： {a}")
    print("----" * 10, "quick_order_3  (快速排序)")
    a = [3, 4, 5, 8, 6, 9, 8, 11, 2, 5, 8]
    print(f"数组为：{a}")
    quick_order_3(a, 0, len(a) - 1)
    print(f"按照快速排序将数组排序后的结果： {a}")

    print(floor(random() * 3))
    ls = [floor(random() * 3) for i in range(10)]
    print(ls)
