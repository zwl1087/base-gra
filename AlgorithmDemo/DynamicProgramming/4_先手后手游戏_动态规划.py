# -*- coding: UTF-8 -*-
"""
@Project ：baseGra 
@File    ：4_先手后手游戏_动态规划.py
@Author  ：赵文龙 
@Date    ：2023/10/27 23:13 
@功能描述

"""


def way3(arr):
    N = len(arr)

    f_map = [[arr[i] if i == j else 0 for i in range(N)] for j in range(N)]
    g_map = [[0 for i in range(N)] for j in range(N)]

    for i in range(1, N):
        row = 0
        col = i
        while col < N:
            f_map[row][col] = max(arr[row] + g_map[row + 1][col], arr[col] + g_map[row][col - 1])
            g_map[row][col] = min(f_map[row + 1][col], f_map[row][col - 1])

            col += 1
            row += 1

    return max(f_map[0][N-1], g_map[0][N-1])


if __name__ == '__main__':
    arr = [50, 100, 20, 10]
    print(way3(arr))
