from datetime import datetime


# 定义一个类
class Tool(object):
    # 定义类对象
    count = 0

    # 定义类方法,一个参数时cls: 类方法可以操作类属性，不可以操作实例属性
    @classmethod
    def get_class_use_times(cls):
        return cls.count

    # 实例化时会自动执行该方法
    def __init__(self, tool_name, tool_type):
        # 定义实例对象: 实例对象必须在 init 函数内部定义
        self.tool_type = tool_type
        self.name = tool_name

        # 类属性的调用
        Tool.count += 1

    # 定义实例方法，第一个参数时self
    def get_tool_type(self, name):
        if name == self.name:
            return self.tool_type
        else:
            print("this tool name has no type define")
            return

    # 定义静态方法，第一个关键字为空： 既不访问类属性、也不访问实例属性
    @staticmethod
    def get_os_time():

        return datetime.now()


if __name__ == '__main__':
    print(Tool.get_os_time())
    chuizi = Tool("锤子", "工程类")
    jisuanji = Tool("计算机", "科学类")

    print(Tool.count)
    print(Tool.get_class_use_times())
    print(chuizi.get_tool_type("榔头"))
    print(chuizi.get_tool_type("锤子"))

    a = 10
    b = 20
    c = 15
    a, b, c = c, b, a
    print(a, b, c)


