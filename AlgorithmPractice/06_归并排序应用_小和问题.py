"""
小和问题：
在一个数组中，每一数左边比当前数小的累加起来，就叫做这个数组的小和
【求解思路1】
    先计算每一个数的小和： 寻找左侧比当前数小的数进行累加
    将每个数的小和加来得到数组的小和
-- =========================================
[1, 3, 4, 2, 5]
1 :  0          = 0
3:   1          = 1
4:   1+3        =4
2:   1          = 1
4:   1+2+3+4    = 10
小和： 0+1+4+1+10 = 16
-- =========================================
[1, 3, 4, 2, 5]
1： 4*1 =4    1的右侧有4个数比1大
3： 2*3 =6    3的右侧有2个数比3大
4： 1*4 =4    4的右侧有1个数比4大
2： 1*2 =2    2的右侧有1个数比2大
5： 0*5 =0    5的右侧有0个数比5大
小和： 4+6+4+2+0 = 16
-- =========================================
求解思路：
    1、中点递归进行求解，将数组拆分
    2、拆分子数组只有一个元素时，停止拆分，递归结束
    3、子数组合并的过程中计算，左侧 < 右侧时候，计算小和： 【当前数的小和 = 其右侧比他大的数的个数 * 当前数】
    4、逐层合并，直至到达到求解的最大长度
    5、最后将 左侧递归求解的结果、右侧递归求解的结果、merge操作求解的结果返回

时间复杂度: O(N*logN)
空间复杂度：O(N)

"""


def get_small_sum(array: list, l, r):
    if l == r:
        return 0
    mid = l + ((r - l) >> 1)
    return get_small_sum(array, l, mid) + get_small_sum(array, mid + 1, r) + merge(array, l, mid, r)


def merge(array, l, mid, r):
    help_list = [0 for i in range(r - l + 1)]
    i = 0
    p1 = l
    p2 = mid + 1
    res = 0
    while p1 <= mid and p2 <= r:
        # 左侧数据严格小于右侧时，计算小和，并且切换指针
        if array[p1] < array[p2]:
            # 按照下标快速计算小和， 当前数的小和 = 其右侧比他大的数的个数 * 当前数
            res += (r - p2 + 1) * array[p1]
            help_list[i] = array[p1]
            i += 1
            p1 += 1
        else:
            # 左侧大于等于右侧时，将右侧的数据先放入辅组列表中，
            # 【注意】： 等于时，必须是先将右侧放入辅组列表，否则会影响到小和值的计算
            help_list[i] = array[p2]
            i += 1
            p2 += 1

    # 如果越界，则不存在大小比较，不计算小和
    while p1 <= mid:
        help_list[i] = array[p1]
        p1 += 1
        i += 1

    while p2 <= r:
        help_list[i] = array[p2]
        i += 1
        p2 += 1
    # 排序很重要，目的是能够将 按照右侧区间的下标快速计算小和
    for i in range(len(help_list)):
        array[l + i] = help_list[i]

    return res


a = [1, 3, 4, 2, 5]
print(get_small_sum(a, 0, 4))

