"""
【流程】
在数组的 l、r 区间内进行排序
    1、取l、r的中点m： l + ((r-l)/2 >> l)
    2、分别在（l、m）区间、（m+1、r）上排序
    3、然后在（l、m）区间上merge插入（m+1，r）上的元素
    4、插入完成后使得（l、r）区间上整体有序

merge 插入过程：
    定义一个辅组数组空间，长度（R-L）+1
    将指针分别指在（l、m） P1、（m+1、r） P2区间的第一个元素，开始比较大小
    P1 > P2，将 P1 指向的元素移入辅组空间，并且移动P1到下一个元素，P2指向不变
    P1 = P2，将 P1 指向的元素移入辅组空间，并且移动P1到下一个元素，P2指向不变
    P1 < P2，将 P2 指向的元素移入辅组空间，并且移动P2到下一个元素，P1指向不变
   如果 P1 越界，那么将P2 剩余的元素全部拷贝到辅助空间
   如果 P2 越界，那么将P1 剩余的元素全部拷贝到辅助空间

最后： 将辅组空间元素回写到初始的数组中

1、递归 【 先按规则拆分（中点拆分），再将元素按规则 聚合 】
2、merge
3、merge and sort


时间复杂度：  O(N*logN)
"""


def order_by_recursion(array: list, l, r):
    if l == r:
        return

    mid = l + ((r - l) >> 1)
    order_by_recursion(array, l, mid)
    order_by_recursion(array, mid + 1, r)
    merge(array, l, mid, r)


def merge(array, l, mid, r):
    # 定义一个临时列表，列表的长度是 r-l+1
    h_arr = [0 for i in range(r - l + 1)]
    i = 0
    p1 = l
    p2 = mid + 1
    # 将左、右区间的数据依次放入临时列表中
    while p1 <= mid and p2 <= r:
        # 将小的先放入列表
        if array[p1] <= array[p2]:
            h_arr[i] = array[p1]
            p1 += 1
            i += 1
        else:
            h_arr[i] = array[p2]
            p2 += 1
            i += 1
    # 右测区间已经没有元素，左侧还有，那么就将左侧的全部复制到临时列表中
    while p1 <= mid:
        h_arr[i] = array[p1]
        p1 += 1
        i += 1
    # 左侧区间已经没有元素，右侧还有，那么就将右侧的全部复制到临时列表中
    while p2 <= r:
        h_arr[i] = array[p2]
        p2 += 1
        i += 1

    # 最后将临时列表有序的元素以此替换到原列表中
    for i in range(len(h_arr)):
        array[l + i] = h_arr[i]


def merge_sort(array: list, left: int, right: int):
    """
    【时间复杂度】：
    【空间复杂度】： O(1)
    :param array:
    :param left:
    :param right:
    :return:
    """
    print("------begin-------")
    if left == right:
        print(f"left = {left}; right = {right} array[left] = {array[left]}")
        return

    mid = left + ((right - left) >> 1)
    print(f"left = {left}; mid = {mid}; right = {right}")
    print("left 递归遍历")
    merge_sort(array, left, mid)
    print("right 递归遍历")
    merge_sort(array, mid + 1, right)
    print("-----end------")

    tmp_list = []
    p1 = left
    p2 = mid + 1
    while p1 <= mid and p2 <= right:
        if array[p1] < array[p2]:
            tmp_list.append(array[p1])
            p1 += 1
        else:
            tmp_list.append(array[p2])
            p2 += 1
    while p1 <= mid:
        print(array[p1])
        tmp_list.append(array[p1])
        p1 += 1
    while p2 <= right:
        print(array[p2])
        tmp_list.append(array[p1])
        p2 += 1

    for i in range(len(tmp_list)):
        array[left + i] = tmp_list[i]


#
# a = [1, 23, 47, 31, 44, 76, 19, 24]
# order(a, 0, 7)


if __name__ == '__main__':
    a = [8, 2, 3, 1, 8, 5, 3, 4, 9, 6, 6, 3, 6, 1]
    print(a.sort())
    order_by_recursion(a, 2, 10)
    print(a)
    a = [8, 2, 3, 1, 8, 5, 3, 4, 9, 6, 6, 3, 6, 1]
    merge_sort(a, 0, len(a) - 1)
    print(a)

    a = [8, 2, 3, 1, 8, 5, 3, 4, 9, 6, 6, 3, 6, 1]
    a.sort()
    print(a)

    # [8, 2, 3, 1, 8, 5, 3, 4, 9, 6, 6, 3, 6, 1]
    # [8, 2, 1, 3, 3, 4, 5, 6, 8, 9, 6, 3, 6, 1]
