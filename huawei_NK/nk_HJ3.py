# -*- coding: utf-8 -*-
"""
@Time ： 2023/7/12 23:58
@Auth ： zhaowenlong
@File ：nk_HJ3.py
@Software: PyCharm
"""
"""
描述
明明生成了 N 个1到500之间的随机整数。请你删去其中重复的数字，即相同的数字只保留一个，把其余相同的数去掉，然后再把这些数从小到大排序，按照排好的顺序输出。

数据范围： 1 ≤ N ≤ 1000
输入的数字大小满足: 1 ≤ value ≤ 500
 
输入描述：
第一行先输入随机整数的个数 N 。 接下来的 N 行每行输入一个整数，代表明明生成的随机数。 具体格式可以参考下面的"示例"。
输出描述：
输出多行，表示输入数据处理后的结果
-- ============================================================================================================================
输入：
    3
    2
    2
    1
输出：
    1
    2 
-- ============================================================================================================================
说明：
输入解释：
第一个数字是3，也即这个小样例的N=3，说明用计算机生成了3个1到500之间的随机整数，接下来每行一个随机数字，共3行，也即这3个随机数字为：
2
2
1
所以样例的输出为：
1
2 
-- ============================================================================================================================
"""

# data = []
# while True:
#     try:
#         N = int(input())
#         if 1 <= N <= 1000:
#             i = 0
#             for i in range(N):
#                 input_val = int(input())
#                 if 0 <= input_val <= 500:
#                     data.append(input_val)
#                 else:
#                     break
#             data_sort = list(set(data))
#             data_sort.sort()
#             for item in data_sort:
#                 print(item)
#         else:
#             break
#     except (EOFError, KeyError):
#         break
#
# data = []
# while True:
#     try:
#         N = int(input())
#         i = 0
#         for i in range(N):
#             data.append(int(input()))
#
#         data_sort = list(set(data))
#         data_sort.sort()
#         for item in data_sort:
#             print(item)
#
#     except (EOFError, KeyError):
#         break

data = {
    6: None,
    5: None,
    3: None,
}
data = sorted(data)
print(data)

data = {}
while True:
    try:
        N = int(input())
        i = 0
        for i in range(N):
            data[int(input())] = ""

        data = sorted(data)
        for item in data:
            print(item)

    except (EOFError, KeyError):
        break
