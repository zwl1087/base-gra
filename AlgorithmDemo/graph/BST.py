# -*- coding: UTF-8 -*-
"""
@Project ：baseGra 
@File    ：BST.py
@Author  ：赵文龙 
@Date    ：2023/10/16 22:55 
@功能描述
图数据结构的宽度遍历

准备条件：
    1、准备一个集合，用来存放已经访问过的节点 visited
    2、准备一个队列，实现先进先出的遍历元素 queue

原理： 从起始节点开始遍历图，
    1、将起始节点放入队列，将起始节点放入已访问节点的集合中
    2、循环开始，队列不为空时，继续遍历 len(queue) > 0
    3、首先，cur=queue.pop(),
    4、同时遍历cur的相邻节点G[cur]:
        如果相邻节点已经被访问过，这pass,
        如果没有被访问过，则将相邻节点C放入 visited集合，并且入队 queue.append(C)

    5、遍历完相邻节点后，打印当前节点cur

实现功能：
    先访问起始节点
    然后访问和起始节点相邻的节点
    再访问和相邻节点相邻的节点
    .....
    直到所有节点都被访问完全

    ************************************************
    特点： 起始节点的子节点先进先出， 因此深度遍历用队列
    ************************************************

"""
graph = {
    "A": ["B", "C"],
    "B": ["A", "C", "D"],
    "C": ["A", "B", "D", "E"],
    "D": ["B", "C", "E", "F"],
    "E": ["C", "D"],
    "F": ["D"]
}


def bst(graphs, start_node):
    queue = [start_node]
    seen = set()
    seen.add(start_node)

    while len(queue) > 0:
        cur = queue.pop(0)
        for c in graphs.get(cur):
            if c not in seen:
                queue.append(c)
                seen.add(c)
        print(cur, end=" ")


bst(graph, "A")
