# 定义
demo_list = [1, 2, 4, 'a', [], 'abc', (1, 2, 3), {"name": "xiaonaofu"}]
# 定义空列表
demo_none_list = []

# 【常用操作】
# 0、查找 ===============================================
# 查询指定位置的元素
print(f"获取列表的元素： {demo_list[1]}")
print(f"获取最后一个元素： {demo_list[-1]}")
try:
    print(demo_list[100])  # 报错：越界
except Exception as e:
    print("ERROR: ", e)
# 获取元素的索引
demo_list.index('abc')
# demo_list.index('赵文龙')
try:
    demo_list.index('赵文龙')
except ValueError as e:
    print("ERROR: ", e)

# 1、修改 ===============================================
# 修改， 如果下表不存在时，就会报错
demo_list[0] = 'zhaowenlong'
# 2、插入 ===============================================
# 追加
demo_list.append("append")
# 在指定位置插入数据
demo_list.insert(2, "insert_value")

# 把其他列表中的内容完整的追加到列表当中，不限于列表，可迭代对象的内容就可以追加
extend_list = ["1@", "tmep@"]
demo_list.extend(extend_list)

print(demo_list)

# 3、删除 ==================================================
# pop : 1、删除最后一个元素; 2、根据索引删除任意位置的元素
demo_list.pop()
print(demo_list)
demo_list.pop(2)
print(demo_list)
# remove : 根据元素的内容删除
# 【1】Remove first occurrence of value.
# 【2】Raises ValueError if the value is not present.
demo_list.remove(4)
print(demo_list)
# del : 将一个变量从内存中删除;
del demo_list[0]
print(demo_list)
# clear ： 删除全部
demo_list.clear()
print(demo_list)

# 4、统计 ==================================================
demo_list_1 = ["a", "ac", "a", "b", "c", "a", "b", "c"]
# 获取列表的长度
print(len(demo_list_1))
# 获取元素出现的频次
print(demo_list_1.count("a"))

# 5、排序
# sort 只适用于对元素类型相同的列表进行排序
# 如果元素类型不同，就会报错
demo_list_1.sort()
print(demo_list_1)
demo_list.sort()
print("demo_list: ", demo_list)

demo = (1, 2, 3, 4, 5)
demo = [1, 2, 3, 4, 5]
from functools import reduce

print(reduce(lambda x, y: x + y, demo))

print([-1] * 5)
demo2 = [5, 6]
print(demo - demo2)
