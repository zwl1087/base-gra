# -*- coding: utf-8 -*-
"""
@Time ： 2022/12/17 0:19
@Auth ： zhaowenlong
@File ：proc_transfer.py
@Software: PyCharm

【进程间通信】
    定义进程间通信的Queue:  多个进程间共享一个Queue, 从而完成进程间的通信

"""
import os
from multiprocessing import Process, Queue
from time import sleep


def download(download_files, proc_queue: Queue):
    for file in download_files:
        print(f"{file} is downloading now...")
        proc_queue.put(file)


def save_docx(proc_queue: Queue):
    while True:
        sleep(1)
        try:
            file = proc_queue.get(timeout=3)
            if file.split(".")[1] == "docx":
                print(f"[save_docx: {os.getpid()}] task saved {file} success !")
            else:
                proc_queue.put(file)
        except Exception as e:
            print("All files had already download success")
            break


def save_pic(proc_queue: Queue):
    while True:
        sleep(1)
        try:
            file = proc_queue.get(timeout=3)
            if file.split(".")[1] == "jpg":
                print(f"[save_pic: {os.getpid()}] task saved {file} success !")
            else:
                proc_queue.put(file)
        except Exception as e:
            print("All files had already download success")
            break


def save_xlsx(proc_queue: Queue):
    while True:
        sleep(1)
        try:
            file = proc_queue.get(timeout=3)
            if file.split(".")[1] == "xlsx":
                print(f"[save_xlsx: {os.getpid()}] task saved {file} success !")
            else:
                proc_queue.put(file)
        except Exception as e:
            print("All files had already download success")
            break


if __name__ == '__main__':
    # 定义进程间通信的Queue:  多个进程间共享一个Queue, 从而完成进程间的通信
    queue = Queue()
    files = ["zhaowenlong.jpg", "huqingling.xlsx", "xiaoxiaonaofu.docx"]
    p1 = Process(target=download, args=(files, queue))
    p2 = Process(target=save_docx, args=(queue,))
    p3 = Process(target=save_pic, args=(queue,))
    p4 = Process(target=save_xlsx, args=(queue,))
    p1.start()
    p2.start()
    p3.start()
    p4.start()
    # 进程阻塞, 使主进程处于等待子进程执行的状态
    p4.join()

    print("download jod run success, its over now")
