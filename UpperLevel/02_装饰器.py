import datetime
import numbers
import operator
from functools import reduce
import time
from typing import List, Any

"""
装饰器就是一种闭包; 简化了闭包的调用
"""


# 定义一个装饰器
def logger(func):
    def inner():
        print("======开始执行了======")
        # 内函数执行了函数体
        func()
        print("======执行结束了======")

    return inner


def timer(func):
    def inner():
        start_time = datetime.datetime.now()
        func()
        end_time = datetime.datetime.now()
        print(f"{func.__name__} 执行的时长是{end_time - start_time}")

    return inner


# 被调用函数有参数，则需要给内函数添加形参
# 内函数调用函数时，进行参数传递
# 被调用函数有参数，那么他所有的装饰器都必须有参数
def args(func):
    def inner(*args, **kwargs):
        print(f"参与计算的两个数是： {args},{kwargs}")
        import numbers
        flag = False

        for i in args:
            print(i)
            if isinstance(i, numbers.Number):
                flag = True
                print(flag)
            else:
                flag = False
                break
        if flag:
            func(*args, **kwargs)
        else:
            print(" wrong input ")

    return inner


# 1、装饰器的应用： @装饰器名；
# 2、放在需要执行函数的前面；
# 3、等价于： demo_func = logger(demo_func)
# 4、多个装饰器的执行顺序： 靠近函数的先执行
@logger
@timer
def demo_func():
    # time.sleep(2)
    print(f"this is {demo_func.__name__} is running now!")


@args
def add(x, y):
    print(f"结果是:  {x + y}")


if __name__ == '__main__':
    demo_func()
    # add(1, "2")

    def is_all_number(check_list):
        res = set(list(map(lambda x: isinstance(x, numbers.Number), check_list)))
        if False in res or len(res) > 1:
            return False
        else:
            return True


    def is_list_same(list1: List[Any], list2: List[Any]) -> bool:

        flag = False
        if operator.eq(list1, list2):
            flag = True
        return flag


    print(is_all_number([1, 2, 3]))
    print(is_all_number([1, 2, "a"]))

    print('-----------' * 10)
    l1 = [1, 2, 3, 4]
    l2 = [1, 2, 3, 4]
    print(operator.eq(l1, l2))
    print(is_list_same(l1, l2))
