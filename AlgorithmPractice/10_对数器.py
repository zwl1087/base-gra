# -*- coding: utf-8 -*-
"""
@Time ： 2023/7/5 22:36
@Auth ： zhaowenlong
@File ：10_对数器.py
@Software: PyCharm
"""
import math
from random import random

# 等概率返回[0,1)上的任意一个数据
print(random())
# 等概率返回[0,8)上的任意一个数据
print(random() * 8)


def f1():
    times = 1000000
    k = 9
    counts = [0] * k
    print(counts)
    for i in range(times):
        ans = int(random() * k)
        counts[ans] += 1

    print(counts)


# ======================================================================================================================
# 返回一个[0,1)的数x，使其出现的概率是x的平方

def f2():
    """
    将在[0,1) 上返回的一个等概率的随机数记为一个独立事件；那么要实现[0,1)上等返回一个概率为x^2的数，就必须触发两次独立事件发生
    max() 能实现其中的两个random()都被执行，因此得到概率是x^2
    :return:
    """
    return max(random(), random())


print(f2())


# times = 1000000
# x = 0.5
# count = 0
# for i in range(times):
#     ans = f2()
#     if ans < x:
#         count += 1
#
# print(count/times)

# ======================================================================================================================
# 定义一个返回[1,5]的函数
def f15():
    return math.floor(random() * 5 + 1)


def g01():
    ans_1 = 0
    while True:
        ans_1 = f15()
        if ans_1 == 3:
            continue
        else:
            return 0 if ans_1 < 3 else 1


def g07():
    return (g01() << 2) + (g01() << 1) + g01()


def g06():
    ans_2 = 0
    while True:
        ans_2 = g07()
        if ans_2 == 7:
            continue
        else:
            return ans_2


def g17():
    return g06() + 1


print(g01())
print("g06()", g06())


# times = 1000000
# counts = [0] * 8
# print(counts)
# for i in range(times):
#     ans = g17()
#     counts[ans] += 1
#
# for j in range(len(counts)):
#     print(j, "----", counts[j])


# ======================================================================================================================
# 定义一个返回 0,1 不等概率的函数
def diff_01():
    return 0 if random() < 0.84 else 1


# 获取一个函数：使得其等概率的返回0，1两个数字
def same_01():
    """
    解题思路：
        1、分别使用diff_01() 产生两个数，假设diff_01()产生0的概率为 p, 产生1的概率为 1-p
        2、当获取结果都是0， 或者都是1时，重新获取，直至得到01，或者10；
        3、当获取结果为01时，则返回 0
        4、当获取结果为10时，则返回 1
        5、此时将等概率的获取0，1， 获取的概率为p(1-p)

    :return:
    """
    same_ans = 0
    while True:
        same_ans = diff_01()
        if same_ans == diff_01():
            continue
        return same_ans


# times = 1000000
# counts = [0] * 2
# print(counts)
# for i in range(times):
#     ans = same_01()
#     counts[ans] += 1
#
# for j in range(len(counts)):
#     print(j, "----", counts[j])


# ======================================================================================================================
# 实现对数器

def random_len_val_array(max_len, max_value):
    """
    生成一个元素类型为整形的随机数据

    :param max_len: 数组的最大长度
    :param max_value: 数组的最大值
    :return:
    """
    arr = [0] * math.floor(random() * max_len)

    for index in range(len(arr)):
        arr[index] = math.floor(random() * max_value)

    return arr


def is_sorted_array(arr: list):
    """
        判断一个数据是否有序, 即升序

    :param arr:
    :return:
    """
    max_val = arr[0]
    i = 1
    for i in range(len(arr)):
        if max_val > arr[i]:
            return False
        max_val = max(max_val, arr[i])
    return True

# 对数器
# 生成一个随机数组a1
# 备份这个随机数据a2
# 分别用不同排序算法进行排序
# 验证排序后的 a1 a2 是否有序

# ======================================================================================================================
