"""
堆数据结构： 用数组实现的完全二叉树结构： 一个连续的数组就可以看成是一个完全二叉树结构

1、完全二叉树：二叉树的每一层节点都是满的或者不满的数都是从左节点开始的
完全二叉树的节点与数据位置关系：
    第 i 个节点： 左孩子的节点[数组的下标] 2*i + 1；有孩子节点[数组的下标]： 2*i + 2； 父节点[数组的下标]： (i-1)/2
    如果结算结果超过数组的长度，则越界，认为该节点元素是不存在的

2、大根堆： 完全二叉树中如果每棵树的最大值都在树的顶部，则称为是 大根堆
3、小根堆： 完全二叉树中如果每棵树的最小值都在树的顶部，则称为是 小根堆

"""


class Heap:

    # 在数组中插入一个数，使其称为大根堆
    @classmethod
    def heap_insert(cls, array: list, index):
        """
        解决问题： 在一个数组中插入一个元素，使其成为大根堆数据结构
        :param array:
        :param index:
        :return:
        """
        # 1、如果当前节点小于其父节点，则停止向上移动
        # 2、如果当前节点已经到达根节点位置时，停止移动
        index_parent = int((index - 1) / 2)
        while array[index] > array[index_parent]:
            array[index], array[index_parent] = array[index_parent], array[index]
            index = int((index - 1) / 2)
            index_parent = int((index - 1) / 2)

    # 将堆中最大值删除后, 将剩余部分继续调整成大根堆
    @classmethod
    def heapify(cls, array: list, index, heap_size):
        """
        将堆中最大值删除后, 将剩余部分继续调整成大根堆
        【思路】：
                1、将index位置的元素删除，此时将array的最后一个元素移动到index
                2、如果左右孩子存在： 则从index开始，对比其值和其左右孩子的最大值的大小
                3、如果最大值大于index位置的元素值，则将最大值和index位置的元素交换
                4、index来到新位置，如果左右孩子存在： 继续找到其左右孩子的最大值进行对比
                5、如果index元素的新位置没有根叶节点了，也就是没有左右孩子了，则对比判断结束
                6、如果index元素的左右孩子最大值比index小，则说明新位置已经构成了大根堆结构，结束判断

        :param array:
        :param index:  开始向下移动的位置
        :param heap_size: 堆数组的大小
        :return:
        """
        # 计算左孩子的位置
        left = index * 2 + 1
        # 如果左孩子不越界，则说明当前节点有左孩子，此时开始循环
        while left < heap_size:
            # left + 1 < heap_size：如果右孩子存在，
            # 则比较左右孩子的大小，将最大值的下标记录在largest中
            largest = left + 1 if left + 1 < heap_size and array[left] < array[left + 1] else left
            # 比较左、右孩子的最大值和父节点的大小，将最大值的下标记录在largest
            largest = largest if array[largest] > array[index] else index
            # 如果最大值的下标是index本身，则说明当前堆结构就是大根堆，不需要移动，退出循环
            if largest == index:
                break
            # 父节点的值小于左、右孩子的最大值,将父节点向下移动至左、右孩子的最大值
            array[largest], array[index] = array[index], array[largest]
            # 值移动完成后，将父节点的下标更新
            index = largest
            # 按照父节点更新的下标找到其左孩子，进入下一次判断
            left = index * 2 + 1

    def array_to_heap(self, array: list):
        for i in range(int(len(array) / 2), -1, -1):
            self.heapify(array, i, len(array))

    # 堆排序 实现
    def heap_sort(self, array: list):
        """
        【思路】：
                1、 将长度为 heap_size 的数组array的每个元素按照大根堆规则插入数据，此时的数组是 大根堆结构，数组的最大值在 0 位置；
                2、 将 0 位置的元素放在数组最后，同时端开与堆的联系(也就是 heap_size -= 1 )，将最后一个元素放在 0 位置
                3、 将 0 位置的元素进行 heapify, 使得新的堆结构任然是 大根堆结构
                4、 重复1、2、3 步骤，直至 heap_size==0 为止，此时得到的数组array即为升序数组
        【时间复杂度】： O(N*logN)
        【空间复杂度】： O(1)
        :param array:
        :return:
        """
        if len(array) < 2:
            return
        for i in range(len(array)):
            self.heap_insert(array, i)
        heap_size = len(array) - 1
        # 初始的0位置元素换到最后，也就是heap_size位置
        array[0], array[heap_size] = array[heap_size], array[0]
        while heap_size > 0:
            # 将 0 位置的元素进行 heapify, 使得新的堆结构任然是 大根堆结构
            self.heapify(array, 0, heap_size)
            # 同时端开与堆的联系(也就是 heap_size -= 1 )
            heap_size -= 1
            array[0], array[heap_size] = array[heap_size], array[0]

    def heap_sort_2(self, array: list):
        """
        【时间复杂度】： O(N*logN)
        【空间复杂度】： O(1)
        :param array:
        :return:
        """
        if len(array) < 2:
            return
        # 更快捷的将array转化成大根堆结构
        self.array_to_heap(array)
        heap_size = len(array) - 1
        # 初始的0位置元素换到最后，也就是heap_size位置
        array[0], array[heap_size] = array[heap_size], array[0]
        while heap_size > 0:
            # 将 0 位置的元素进行 heapify, 使得新的堆结构任然是 大根堆结构
            self.heapify(array, 0, heap_size)
            # 同时端开与堆的联系(也就是 heap_size -= 1 )
            heap_size -= 1
            array[0], array[heap_size] = array[heap_size], array[0]


if __name__ == '__main__':
    h = Heap()

    a = [-2, 9, 3, 5, 0, 8, -28, 275, 4, 6, 2, 7, 1, 89, 23]
    # a = [-2, 9, 3, 5, 0, 8, 4]
    # h.heap_sort(a)
    print(a)
    a = [-2, 9, 3, 5, 0, 8, -28, 275, 4, 6, 2, 7, 1, 89, 23]
    h.array_to_heap(a)
    # [275, 9, 89, 5, 6, 8, 23, -2, 4, 0, 2, 7, 1, -28, 3]
    # [275, 9, 89, 5, 6, 8, 23, -2, 4, 0, 2, 7, 1, -28, 3]
    print(a)
    a = [-2, 9, 3, 5, 0, 8, -28, 275, 4, 6, 2, 7, 1, 89, 23]
    h.heap_sort_2(a)
    print(a)

