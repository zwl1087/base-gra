# -*- coding: UTF-8 -*-
"""
@Project ：baseGra 
@File    ：DFS.py
@Author  ：赵文龙 
@Date    ：2023/10/16 22:55 
@功能描述

图的深度优先遍历

实现功能：
    先访问起始节点
    然后从他的相邻节点开始，继续访问，直到所有的节点都没子节点时，再返回原路返回至最近的一个分叉点
    ....
    直到起始节点的所有子节点都被访问到，遍历结束

    ************************************************
    特点： 起始节点的子节点先进后出， 因此深度遍历用栈
    ************************************************





"""
graph = {
    "A": ["B", "C"],
    "B": ["A", "C", "D"],
    "C": ["A", "B", "D", "E"],
    "D": ["B", "C", "E", "F"],
    "E": ["C", "D"],
    "F": ["D"]
}


def dfs(graph, start_node):
    # 使用list作为栈
    stack = []
    seen = set()
    seen.add(start_node)
    stack.append(start_node)
    while len(stack) > 0:
        cur = stack.pop()
        for c in graph.get(cur):
            if c not in seen:
                stack.append(c)
                seen.add(c)
        print(cur, end=" ")


dfs(graph, "A")
# A C E D F B
