# -*- coding: utf-8 -*-
"""
@Time ： 2023/7/31 23:42
@Auth ： zhaowenlong
@File ：03_队列_python原生队列.py
@Software: PyCharm
"""

from queue import Queue
from multiprocessing import Queue


q = Queue(maxsize=10)

for i in range(10):
    q.put(i)

print(q.qsize())
print(q.empty())
print(q.full())
# print(q.queue)

while not q.empty():
    print('出队：', q.get())
