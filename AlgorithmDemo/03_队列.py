import time

"""
队列： 【先进先出，后进后出】 -- 从头部插入元素，从尾部删除元素
按照实现方式分类： 利用数据实现队列；利用链表实现队列
利用数据实现队列： 可以按索引访问元素，可以操作元素
    1、普通： 从头部删除，从尾部新增， 缺点：队列空间越来越小
    2、优化： 从头部删除，从尾部新增，每次新增之前，都会检查元素是否从头开始，如果不是就将元素挪动到头部： 有点：队列空间利用率高，缺点：时间复杂度高
    3、循环： 在队列空间中循环插入元素，直到空间占满，在队列空间循环删除元素，直到队列的末尾；
利用链表实现队列： 操作元素很复杂
    1、普用链表队列： 如果内存足够大，可以无限的插入元素，每插入一个元素，就用尾指针将内存空间连接起来
"""


class Queue:
    def __init__(self, capacity):
        # 定义队列的容量
        self.n = capacity
        # 定义一个数组
        self.items = [-1] * capacity
        # 定义头尾指针
        self.head = 0
        self.tail = 0

    def enter_queue(self, item):
        # 判断队列是否已满
        if self.tail == self.n:
            return False
        # 将item 入队
        self.items[self.tail] = item
        # 移动尾部指针
        self.tail += 1

    def out_queue(self):
        # 判断队列是否是空的
        if self.head == self.tail:
            return False
        # 将item元素出队
        item = self.items[self.head]
        self.head += 1
        return item


class OptimizeQueue:
    def __init__(self, capacity):
        # 定义队列的容量
        self.n = capacity
        # 定义一个数组
        self.items = [-1] * capacity
        # 定义头尾指针
        self.head = 0
        self.tail = 0

    #  入队时，先判断队列的头部是不是空的，如果是空的，就将元素移动到头部
    def enter_queue(self, item):
        # 判断尾指针是不是在队列最后
        if self.tail == self.n:
            # 同时判断头部指针是否在起始位置
            if self.head == 0:
                # 如果在其实位置，说明是空队列，不需要移动元素
                return False
            # 如果head不在初始位置，那就将从head到tail的元素移动到初始位置
            for i in range(self.head, self.tail):
                # i 从head开始, 将head位置的元素移动到初始位置
                self.items[i - self.head] = self.items[i]
            # 移动完成后将指针位置重置： 【尾指针向前移动head, 头指针移动到初始位置】
            self.tail -= self.head
            self.head = 0
        self.items[self.tail] = item
        self.tail += 1

    def out_queue(self):
        # 判断队列是否是空的
        if self.head == self.tail:
            return False
        # 将item元素出队
        item = self.items[self.head]
        self.head += 1
        return item


class LinkQueue:
    # 定义队列的头尾指针
    def __init__(self):
        self.head = None
        self.tail = None

    def enter_queue(self, item):
        # 先判断队列是否有元素
        if self.tail is None:
            # 【队列没有元素】
            # 申明一个新节点
            new_node = self.Node(item)
            # 尾指针指向第一个元素
            self.tail = new_node
            # 头指针指向第一个元素
            self.head = new_node
        else:
            # 【队列已经存在元素】
            # 申明一个新节点
            new_node = self.Node(item)
            # 将上一个的next指向新节点
            self.tail.next = new_node
            # 将队列的尾指针移到最后
            self.tail = self.tail.next

    def out_queue(self):
        # 判断是否是空队列，如果是不做操作
        if self.head is None:
            return None
        # 如果队列非空，则将头部节点出队
        old_node = self.head.data
        self.head = self.head.next
        return old_node

    # 定义节点类：包含数据属性和尾指针属性 【内部类】
    class Node:
        def __init__(self, data):
            self.data = data
            self.next = None


class CircleQueue:
    def __init__(self, capacity):
        # 定义队列的容量
        self.n = capacity
        # 定义一个数组
        self.items = [-1] * capacity
        # 定义头尾指针
        self.head = 0
        self.tail = 0

    def enter_queue(self, item):
        if (self.tail + 1) % self.n == 0:
            return False
        self.items[self.tail] = item
        self.tail = (self.tail + 1) % self.n
        return True

    def out_queue(self):
        if self.head == self.tail:
            return None
        item = self.items[self.head]
        self.head = (self.head + 1) % self.n
        return item
