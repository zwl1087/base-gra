from functools import partial

"""
所谓偏函数： 就是固定一个函数的一个或者多个参数，返回一个新的函数，这个函数用于接受剩余的参数
"""

# 设置int函数的进制为2, 生成一个新的函数 new_int，可以将二进制数据转换成十进制数
new_int = partial(int, base=2)
print(new_int("10101"))

# 设置print函数的结尾符为 "@" 生成一个新的 new_print 函数

new_print = partial(print, end="@")
new_print("123")
new_print("123_xiaonaofu")
new_print("123_zhaowenlong")
