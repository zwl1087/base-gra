# -*- coding: utf-8 -*-
"""
@Time ： 2022/12/15 0:40
@Auth ： zhaowenlong
@File ：proc_pool.py
@Software: PyCharm

进程池的使用

"""
import os
import random
import time
from multiprocessing import Pool


def async_task(task_name):
    print(f"[{os.getpid()}] 开始学习{task_name} 课程了")
    start_time = round(time.time(), 3)
    ts = round(random.random() * 5)
    time.sleep(ts)
    end_time = round(time.time(), 3)
    times = end_time - start_time
    msg = f" {task_name} 课程学习结束了, 学习时间是：{round(times * 15)} 学时 ！"
    return msg


def sync_task(task_name):
    print(f"=========> 同步阻塞式任务{task_name} 开始执行了 [{os.getpid()}]")
    start_time = round(time.time(), 3)
    ts = round(random.random() * 5)
    time.sleep(ts)
    end_time = round(time.time(), 3)
    times = end_time - start_time
    msg = f" {task_name} 课程学习结束了, 学习时间是：{round(times * 15)} 学时 ！"
    print(msg)


def callback(n):
    print(n)


# 异步非阻塞方式创建进程：
def async_proc(proc_num, proc_tasks, proc_target, proc_callback):
    pool = Pool(proc_num)
    # tasks = ["Python", "Java", "Js"]
    for task_name in proc_tasks:
        # 异步非阻塞方式创建进程：
        # 参数1： 进程执行的任务, 参数2：任务的参数; 参数3：任务执行完成后的执行的动作
        pool.apply_async(proc_target, args=(task_name,), callback=proc_callback)

    # 表示进程池添加进程结束
    pool.close()
    # 表示开始执行进程池中的进程，同时让主进程处于等待状态
    pool.join()


# 同步阻塞方式创建进程：
def sync_proc(proc_num, proc_tasks, proc_target):
    pool = Pool(proc_num)
    # tasks = ["Python", "Java", "Js"]
    for task_name in proc_tasks:
        # 同步阻塞方式创建进程：
        # 参数1： 进程执行的任务, 参数2：任务的参数; 参数3：阻塞方式没有回调函数
        pool.apply(proc_target, args=(task_name,))

    # 表示进程池添加进程结束
    pool.close()
    # 表示开始执行进程池中的进程，同时让主进程处于等待状态
    pool.join()


if __name__ == '__main__':
    tasks = ["Python", "Java", "Js", "Php", "Golang", "ruby", "C", "C++", "C#"]
    # 创建进程池： 定义进程池的最大个数
    async_proc(5, tasks, async_task, callback)
    print("=========> async_proc 执行结束了")
    sync_proc(5, tasks, sync_task)
    print("=========> sync_proc 执行结束了")

    print(f"{os.getpid()} 执行结束了")
