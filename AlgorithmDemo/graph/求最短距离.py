# -*- coding: UTF-8 -*-
"""
@Project ：baseGra 
@File    ：求最短距离.py
@Author  ：赵文龙 
@Date    ：2023/10/17 0:03 
@功能描述

图的宽度优先遍历时，遍历相邻节点时，本身时没顺序的，因此宽度优先遍历回得到多组解
加权图： 已指定具有适当权重的边的图形称为加权图形。加权图可以进一步分类为有向加权图和无向加权图。
如果宽度优先遍历

"""
import math
from queue import PriorityQueue

graph = {
    "A": {"B": 5, "C": 1},
    "B": {"A": 5, "C": 2, "D": 1},
    "C": {"A": 1, "B": 2, "D": 4, "E": 8},
    "D": {"B": 1, "C": 4, "E": 3, "F": 6, },
    "E": {"C": 8, "D": 3},
    "F": {"D": 6},
}


def bst_distance(graphs, s):
    p_queue = PriorityQueue()
    p_queue.put((0, s))

    seen = set()

    parent = {s: None}

    # 保存所有点到S点的距离，如果时S点，则距离为0
    distance = {k: 0 if k == s else math.inf for k in graph.keys()}
    while not p_queue.empty():
        # print(p_queue.get())
        item = p_queue.get()
        dist = item[0]
        cur = item[1]
        # print(dist, cur)
        seen.add(cur)
        for c in graphs.get(cur).keys():
            if c not in seen and dist + graphs.get(cur).get(c) < distance[c]:
                p_queue.put((graphs.get(cur).get(c) + dist, c))
                distance[c] = dist + graphs.get(cur).get(c)
                parent[c] = cur
    print("-----------")
    v = "C"
    while v:
        print(v)
        v = parent[v]
    print("--------------")

    return parent, distance


if __name__ == '__main__':
    print(f"结果： \n {bst_distance(graph, 'F')}")
