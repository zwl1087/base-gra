# -*- coding: UTF-8 -*-
"""
@Project ：baseGra 
@File    ：8_求共同最大值子序列长度_递归.py
@Author  ：赵文龙 
@Date    ：2023/10/31 23:20 
@功能描述

"""

"""
有 str1 和 str2 两个字符串， 求他们的最大公共子序列的长度
子序列： 顺序一直，可以连续，也可以不连续， 区别于子串，必须时连续的


"""


def way1(s1, s2):
    if s1 is None or len(s1) == 0 or s2 is None or len(s2) == 0:
        return 0

    def process(str1, str2, i, j):
        """
         返回 str1[0-i] 和 str2[0-j]上的 最长公共子序列的长度
        """

        if i == 0 and j == 0:
            # print(f"i == 0 and j == 0:  {1 if str1[i] == str2[j] else 0}")
            return 1 if str1[i] == str2[j] else 0

        elif i == 0:
            # print("i == 0")
            if str1[i] == str2[j]:
                return 1
            else:
                return process(str1, str2, i, j - 1)
        elif j == 0:
            # print("j == 0")
            if str1[i] == str2[j]:
                return 1
            else:
                return process(str1, str2, i - 1, j)
        else:
            # print("i != 0 and j != 0")
            p1 = process(str1, str2, i - 1, j)
            p2 = process(str1, str2, i, j - 1)
            p3 = 1 + process(str1, str2, i - 1, j - 1) if str1[i] == str2[j] else 0
            # print(p1, p2, p3)
            return max(p1, p2, p3)

    return process(s1, s2, len(s1) - 1, len(s2) - 1)


if __name__ == '__main__':
    s1 = "a123cb"
    s2 = "b123db"
    print(way1(s1, s2))
