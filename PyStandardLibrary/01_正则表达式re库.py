import re

"""
re python的正则表达式标准库
1、正则表达式是在字符串中查找满足条件的字符串并返回满足条件的字符串
    re.match()
    re.fullmatch() 全词匹配，如果能匹配到，则返回匹配结果，如果匹配不到，则返回None
    re.search()
    re.findall()
    re.finditer()
2、对满足条件的字符串进行操作
    re.split()
    re.sub()
    re.subn()
3、编译
    re.compile()
4、转义正则表达式
    re.escape()
5、清除缓存
    re.purge()
"""

# print(dir(re))
# 1、 re.match : 从字符串的开始位置匹配，如果匹配成功，则字符串，如果匹配失败则返回 None
str_1 = r"wo men shi gong chan zhu yi jie ban ren"
str_2 = r"men shi gong chan zhu yi jie ban ren"
str_3 = r"sunk is good man"

patt_1 = "wo"
patt_2 = "go"
patt_3 = "Wo"
patt_4 = "man"

print("---" * 30, "re.match")
o_1 = re.match(patt_1, str_1)
print(o_1.group())
print(o_1.start())

o_2 = re.match(patt_2, str_1)
print(o_2)

o_3 = re.match(patt_3, str_1, flags=re.I)
# 返回匹配成功的结果在原字符串的位置
print(o_3.span())
print(o_3.group())

print("---" * 30, "re.search")
s_4 = re.search(patt_4, str_3)
print(s_4)
print(s_4.groups())
print(s_4.group())
print(s_4.group(0))
print("匹配字符串的开始位置：  s_4.start() = ", s_4.start())
print("匹配字符串的结束位置：  s_4.end() = ", s_4.end())

print("---" * 30, "re.findall")
# 查询全部的匹配对象
phones = "010-23456789#0349-88108234#0750-66253488#02a-23459876#0750-66253489"
patt_all = r"(\d{4}|\d{3})-(\d{8})"
# findall 返回一个列表
print("findall()的返回结果是： ", re.findall(patt_all, phones))

print("---" * 30, "re.分组概念")
# 正则表达式除了有判断是否匹配之外，还有提取子字符串的功能，用()表示的就是提取分组
phone_no = "0750-23388888"
# 用()来表示分组
patt_5 = r"(\d{4})-(\d{8})"
# 查看匹配的各组的情况
print(re.match(patt_5, phone_no).groups())
# group(0) 表示 匹配到的完整的、原始的字符串
print(re.match(patt_5, phone_no).group(0))
# 第一分组
print(re.match(patt_5, phone_no).group(1))
# 第二分组，以此类推
print(re.match(patt_5, phone_no).group(2))

# 分组别名:
print("---" * 30, "re.分组别名")
# 语法： 在分组内部： (?P<分组别名>正则表达式)
patt_6 = r"(?P<no_1>\d{4})-(?P<no_2>\d{8})"
print(re.search(patt_6, phone_no).groups())
print(re.search(patt_6, phone_no).group("no_1"))

print("---" * 30, "re.编译")
# 编译
# re模块：
# 1、将正则表达式进行编译，如果正则表达式不合法，则会报错
# 2、用编译后的正则表达式匹配对象

re_phone_6 = re.compile(patt_6)
m = re_phone_6.search(phone_no)
print(m.groups())

print(ord("A"))
print(''.join(["a", "B", "c"]))

times = [1, 2, 3, 4, 5]
print(sum(times) / len(times))

print("4 ^ 4 = ", 4 ^ 4)

a = 126
print("a & (~a + 1) = ", a & (~a + 1))
print(int(2))
