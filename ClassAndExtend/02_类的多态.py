class Dog:
    def __init__(self, name):
        self.name = name

    def game(self):
        print(f"{self.name} is playing now")


class TandiDog(Dog):
    # 子类中重新父类的方法： 实现子类的多态
    def game(self):
        print(f"the dog name is {self.name} ")
        super().game()
        print(f"{self.name} is playing very fun")
        print("=="*20+"===end")


class Person:
    def __init__(self, name):
        self.name = name

    def game_with_dog(self, dog):
        print(f"{self.name} is game with his dog {dog.name} now")
        dog.game()


if __name__ == '__main__':
    jack = Person("小明")
    franck = TandiDog("旺财")
    hose = TandiDog("糊糊")
    jack.game_with_dog(franck)
    jack.game_with_dog(hose)


