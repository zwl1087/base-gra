# -*- coding: UTF-8 -*-
"""
@Project ：baseGra 
@File    ：7_组成目标字符串所需最少贴纸数据_递归.py
@Author  ：赵文龙 
@Date    ：2023/10/30 22:45 
@功能描述

"""
import math
from typing import List

"""
给定一个字符串str, 给定一个字符串类型的数组 ticket_arr, 出现的字符串都是小写英文
ticket_arr 的每个字符串代表一张贴纸，可以把单个字符串剪开使用，目的时拼出str来，
假定 ticket_arr 的每个字符串都有无限多个且可以重复使用
问题：  求至少需要多少张贴纸可以完成任务

str = "babac"  ticket=["ba", "c", "abcd"]

ba + abcd  --> babac
ba + ba + c  --> babac
因此求得最优解 2

"""


def way1(target, tickets):
    def process(targets, tickets):
        if len(targets) == 0:
            return 0
        min_ = math.inf
        for t in tickets:
            rest = min_value(targets, t)
            print(f"1、 t: {t}, rest： {rest}, min_: {min_}")
            if len(rest) != len(targets):
                min_ = min(min_, process(rest, tickets))
                print(f"2、 t: {t}, rest： {rest}, min_: {min_}")

        return min_ + (0 if min_ == math.inf else 1)

    def min_value(target_str, ticket_str):
        countor = [0 for i in range(26)]
        for item in target_str:
            countor[ord(item) - ord("a")] += 1

        for t in ticket_str:
            countor[ord(t) - ord("a")] -= 1

        rest = [chr(index + ord("a")) * val for index, val in enumerate(countor) if val > 0]

        return "".join(rest)

    ans = process(target, tickets)
    return -1 if ans == math.inf else ans


def min_val(target_str, ticket_str):
    countor: list[int] = [0 for i in range(26)]

    item: str
    for item in target_str:
        countor[ord(item) - ord("a")] += 1
    print(countor)
    for t in ticket_str:
        countor[ord(t) - ord("a")] -= 1

    print(countor)

    rest = [chr(index + ord("a")) * val for index, val in enumerate(countor) if val > 0]
    print(rest)

    return "".join(rest)


if __name__ == '__main__':
    print(way1("baaccc", ["ba", "c", "abcd"]))

