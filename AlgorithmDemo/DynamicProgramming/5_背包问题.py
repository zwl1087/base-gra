# -*- coding: UTF-8 -*-
"""
@Project ：baseGra 
@File    ：5_背包问题.py
@Author  ：赵文龙 
@Date    ：2023/10/28 17:43 
@功能描述

"""
"""

1、两个等长整型数组， 一个是重量一个是价值
2、背包最大重量bag
问题： 满足不超重的情况下，如何选取货物能使得价值最大


分析思路:
模型: 从左往右的尝试模型; 

0  1  2

0
    1       
            2
            !2
    !1
            2
            !2
!0
    1       
            2
            !2
    !1
            2
            !2
从当前节点开始尝试，如果分析要和不要当前节点的所有情况，即暴力枚举

"""


def way1(w, v, bag):
    if w is None or v is None or len(w) != len(v) or len(w) == 0:
        return 0

    def process(w_, v_, index, bag_):
        """
        解决从 index 开始往后， 取出的节点满足不超重的情况下还能保证价值最大
        """
        print("index, bag_ : ", index, bag_)
        # base_case: 超重后不在选取
        if bag_ < 0:
            print("出现无效节点了： index, bag_ : ", index, bag_)
            return -1
        # base_case: 列表已经取完了
        if index == len(w_):
            print("没有节点了： index, bag_ : ", index, bag_)
            return 0

        # 还有可选货物的时候
        # 不要当前节点, 最大价值就是下一个节点返回的最大价值
        p1 = process(w_, v_, index + 1, bag_)
        print(f"*****bag为【{bag_}】时不要{index}({w_[index]})节点计算的P1值：{p1}")

        # 如果需要当前节点：
        #   a.并且下一个节点是有效的， 最大价值 = 节点的价值 + 下一个节点返回的价值
        #   b.下一个节点是无效的, 跳出递归, 最大价值 = 0
        p2 = 0
        next_val = process(w_, v_, index + 1, bag_ - w_[index])
        print(f'计算下一个节点{index + 1}【{bag_ - w_[index]}】是否有效：{"无效"if next_val == -1 else "有效" }')
        if next_val != -1:
            print(next_val)
            p2 = v_[index] + next_val

        else:
            print(f"下一个节点: {index, bag_} :  是无效的")
        print(f"*****bag为【{bag_}】时要{index}({w_[index]})节点计算的P2值：{p2} ")
        return max(p1, p2)

    return process(w, v, 0, bag)


if __name__ == '__main__':
    w = [3, 2, 4, 7]
    v = [5, 6, 3, 19]
    bag = 11
    print(way1(w, v, bag))
