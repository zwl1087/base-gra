# -*- coding: utf-8 -*-
"""
@Time ： 2023/2/12 1:12
@Auth ： zhaowenlong
@File ：05_类装饰器.py
@Software: PyCharm
类装饰： 装饰器实现通过类来实现
原理： 重写父类__call__方法
__call__方法的作用：类中一个非常特殊的实例方法，即 __call__()。该方法的功能类似于在类中重载 () 运算符，使得类实例对象可以像调用普通函数那样，以“对象名()”的形式使用。
"""
import time


class Check(object):
    """
    1、 实例化的时候，接收一个被装饰器函数
    2、 重写__call__方法
    3、 在__call__方法中调用被装饰函数
    4、 在__call__方法中实现装饰器的功能
    """

    def __init__(self, func):
        self.__func = func

    def __call__(self, *args, **kwargs):
        # 增加装饰器的逻辑
        print("这是类装饰器")
        # 在__call__方法中调用被装饰函数
        self.__func()


class Timer:
    # 定义被装饰函数的入参
    def __init__(self, prefix):
        self.prefix = prefix

    # __call__ 方法中接收被装饰函数, 同时将__call__方法定义成类似一个装饰器
    def __call__(self, func):
        def wrapper(*args, **kwargs):
            start = time.time()
            ret = func(*args, **kwargs)
            time.sleep(2)
            end = time.time()
            print(f"{self.prefix}, 函数执行时长为：{round(end - start, 4)}")
            # 返回被装饰函数的值
            return ret

        return wrapper


@Check
def comment():
    print(f"这是执行函数")


@Timer(prefix="zhaowenlong")
def add(x: int, y: int):
    return x + y


if __name__ == '__main__':
    comment()
    print(add(3, 5))
