# -*- coding: UTF-8 -*-
"""
@Project ：baseGra 
@File    ：4_先手后手游戏_缓存.py
@Author  ：赵文龙 
@Date    ：2023/10/27 23:13 
@功能描述

"""


def way2(arr):
    N = len(arr)

    f_map = [[-1 for m in range(N)] for n in range(N)]
    g_map = [[-1 for m in range(N)] for n in range(N)]

    def f(arr, L, R, f_map, g_map):
        if f_map[L][R] != -1:
            return f_map[L][R]

        ans = 0
        if L == R:
            ans = arr[L]
        else:
            p1 = arr[L] + g(arr, L + 1, R, f_map, g_map)
            p2 = arr[R] + g(arr, L, R - 1, f_map, g_map)
            ans = max(p1, p2)

        f_map[L][R] = ans

        return ans

    def g(arr, L, R, f_map, g_map):
        if g_map[L][R] != -1:
            return g_map[L][R]

        ans = 0
        if L == R:
            ans = 0
        else:
            p1 = f(arr, L + 1, R, f_map, g_map)
            p2 = f(arr, L, R - 1, f_map, g_map)

            ans = min(p1, p2)
        g_map[L][R] = ans

        return ans

    if arr is None or len(arr) == 0:
        return 0

    return max(f(arr, 0, N - 1, f_map, g_map), g(arr, 0, N - 1, f_map, g_map))


if __name__ == '__main__':
    arr = [50, 100, 20, 10]
    print(way2(arr))
