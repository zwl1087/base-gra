"""
子类有多个父类
"""


class A:
    def test(self):
        print("A----- test function")

    def demo(self):
        print("A----- demo function")


class B:
    def test(self):
        print("B----- test function")

    def demo(self):
        print("B----- demo function")


# 【1】 多继承语法
class C(B, A):

    def c_test(self):
        print("C----- c_test function")


if __name__ == '__main__':
    c = C()
    # 【2】多继承的执行顺序： (<class '__main__.C'>, <class '__main__.B'>, <class '__main__.A'>, <class 'object'>) 以此查找
    print(C.__mro__)
    # 从 <class '__main__.C'> 查找并且执行
    c.c_test()
    # 从 <class '__main__.B'> 查找并执行
    c.test()
    print(dir(A))

