# -*- coding: utf-8 -*-
"""
@Time ： 2023/7/7 22:37
@Auth ： zhaowenlong
@File ：05_二分查找_寻找等于num最左的位置.py
@Software: PyCharm
"""
"""
给定一个有序数据，从数组中查找>num的最左的位置
"""
import math
from random import random


def find_most_left(arr: list, num: int) -> int:
    """

    :param arr:  有序数组
    :param num:  查找的num
    :return:   大于等于num的最左位置的索引;  -1 表示没有这个索引位置
    """
    if arr == [] or arr is None:
        return -1

    left = 0
    right = len(arr) - 1
    # print("left, right: ", left, right)
    ans_index = -1

    while left <= right:
        mid = int((left + right) / 2)
        if arr[mid] >= num:
            right = mid - 1
            ans_index = mid
        else:
            left = mid + 1

    return ans_index


def random_len_val_array(max_len: int, max_value: int, is_sorted: bool = False):
    """
    生成一个元素类型为整形的随机数据

    :param max_len: 数组的最大长度
    :param max_value: 数组的最大值
    :return:
    """
    random_arr = [0] * math.floor(random() * max_len)

    for index in range(len(random_arr)):
        random_arr[index] = math.floor(random() * max_value)

    if is_sorted:
        random_arr.sort()

    return random_arr


if __name__ == '__main__':
    num = 50

    for i in range(100000):
        arr = random_len_val_array(15, 80, True)
        f_index = find_most_left(arr, num)
        l_index = -1
        for j in range(len(arr)):
            if arr[j] >= num:
                l_index = j
                break
        if f_index != l_index:
            print("Fuck, the code has a BUG !")

    print("Well Done, code is perfect !!!")


