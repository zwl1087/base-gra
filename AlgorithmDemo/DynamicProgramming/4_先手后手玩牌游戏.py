# -*- coding: UTF-8 -*-
"""
@Project ：baseGra 
@File    ：4_先手后手玩牌游戏.py
@Author  ：赵文龙 
@Date    ：2023/10/25 23:55 
@功能描述

"""


def f(arr, l, r):
    # print(f"f 函数 l, r: {l, r}")
    if l == r:
        return arr[l]
    l1 = arr[l] + g(arr, l + 1, r)
    l2 = arr[r] + g(arr, l, r - 1)

    return max(l1, l2)


def g(arr, l, r):
    # print(f"g 函数 l, r: {l, r}")
    if l == r:
        return 0
    l1 = f(arr, l + 1, r)
    l2 = f(arr, l, r - 1)
    return min(l1, l2)


def score(arr):
    if arr is None or len(arr) == 0:
        return 0
    fir = f(arr, 0, len(arr) - 1)
    sec = g(arr, 0, len(arr) - 1)
    return max(fir, sec)


if __name__ == '__main__':
    arr = [50, 100, 20, 10]
    print(score(arr))
