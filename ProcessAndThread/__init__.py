# -*- coding: utf-8 -*-
"""
@Time ： 2022/12/14 1:28
@Auth ： zhaowenlong
@File ：__init__.py.py
@Software: PyCharm
"""
import time


def localtime():
    timestamp = time.time()
    struct_time = time.localtime(timestamp)
    tf = time.strftime('%Y-%m-%d %H:%M:%S', struct_time)
    return tf


if __name__ == '__main__':
    print(localtime())
