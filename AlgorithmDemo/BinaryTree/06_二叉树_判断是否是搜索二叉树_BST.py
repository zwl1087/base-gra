# -*- coding: utf-8 -*-
"""
@Time ： 2023/8/19 23:13
@Auth ： zhaowenlong
@File ：06_二叉树_判断是否是搜索二叉树_BST.py
@Software: PyCharm
"""
import sys
from pythonds import Stack
from AlgorithmDemo.BinaryTree import create_binary_tree, Nodes, bst

"""
搜索二叉树：  一个二叉树的任何一个子树都满足 头节点值大于左孩子且小于右孩子的值， 这样的二叉树称为搜索二叉树
盘点依据： 二叉树的中序遍历是一个递增序列

"""


class IsBst:
    min_num = -sys.maxsize - 1

    def is_binary_search_tree(self, head: Nodes):
        if head is None:
            return True

        is_left = self.is_binary_search_tree(head.left)
        if not is_left:
            return False
        print(f"{head.data}, {self.min_num}, {head.data <= self.min_num}")
        if head.data <= self.min_num:
            return False
        else:
            self.min_num = head.data

        is_right = self.is_binary_search_tree(head.right)
        return is_right

    def is_binary_search_tree_unrecur(self, head: Nodes):
        stack = Stack()
        min_value = -sys.maxsize - 1
        if head is not None:

            while not stack.isEmpty() or head is not None:
                if head is not None:
                    stack.push(head)
                    head = head.left
                else:
                    head = stack.pop()
                    if head.data <= min_value:
                        return False
                    else:
                        min_value = head.data
                    head = head.right
        return True


if __name__ == '__main__':
    is_bst1 = IsBst()

    root1 = create_binary_tree()
    print(is_bst1.is_binary_search_tree(root1))

    is_bst2 = IsBst()
    root2 = bst()
    print(is_bst2.is_binary_search_tree(root2))

    isbst = IsBst()
    print(isbst.is_binary_search_tree_unrecur(root1))
