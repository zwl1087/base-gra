# -*- coding: UTF-8 -*-
"""
@Project ：baseGra 
@File    ：5_背包问题_动态规划.py
@Author  ：赵文龙 
@Date    ：2023/10/29 0:58 
@功能描述

"""
"""
暴力递归转动态规划：
1、求重复解 p(index,rest)
2、分析可变参数
3、求可变参数的范围:  考虑边界值
4、分析dp的初始值:  暴力递归的basecase
4、分析dp[][]值的依赖关系： 写出dp生成的逻辑
5、分析返回值

"""


def way2(w, v, bag):
    if w is None or v is None or len(w) != len(v) or len(w) == 0:
        return 0

    N = len(w)

    dp = [[0 for i in range(bag + 1)] for j in range(N + 1)]

    for index in range(N - 1, -1, -1):
        # print(index)
        for rest in range(bag + 1):

            p1 = dp[index + 1][rest]
            p2 = 0
            next_val = -1 if rest - w[index] < 0 else dp[index + 1][rest - w[index]]
            if next_val != -1:
                p2 = v[index] + next_val
            dp[index][rest] = max(p1, p2)
            # print("rest: ", rest, dp[index][rest])

    return dp[0][bag]


if __name__ == '__main__':
    w = [3, 2, 4, 7]
    v = [5, 6, 3, 19]
    bag = 11
    print(way2(w, v, bag))
    for i in range(5, 0, -1):
        print(i)

    print(chr(0))
    print(ord("0"))
    print(ord("F"))
    print()
