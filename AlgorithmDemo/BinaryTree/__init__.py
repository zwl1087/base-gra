# -*- coding: utf-8 -*-
"""
@Time ： 2023/8/19 23:20
@Auth ： zhaowenlong
@File ：__init__.py.py
@Software: PyCharm
"""

from binarytree import Node


class Nodes:
    def __init__(self, data):
        self.data = data
        self.left = None
        self.right = None


def create_binary_tree():
    root = Nodes(0)
    l1 = Nodes(1)
    r1 = Nodes(2)
    l2_l = Nodes(3)
    l2_r = Nodes(4)
    r2_l = Nodes(5)
    r2_r = Nodes(6)
    root.left = l1
    root.right = r1
    l1.left = l2_l
    l1.right = l2_r
    r1.left = r2_l
    r2_l.right = r2_r
    return root


def bst():
    root = Nodes(3)
    l1_l = Nodes(1)
    l1_r = Nodes(5)
    root.left = l1_l
    root.right = l1_r

    l2_l_l = Nodes(0)
    l2_l_r = Nodes(2)
    l1_l.left = l2_l_l
    l1_l.right = l2_l_r

    l2_r_l = Nodes(4)
    l2_r_r = Nodes(6)
    l1_r.right = l2_r_r
    l1_r.left = l2_r_l

    return root
