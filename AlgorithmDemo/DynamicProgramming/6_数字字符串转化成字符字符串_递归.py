# -*- coding: UTF-8 -*-
"""
@Project ：baseGra 
@File    ：6_数字字符串转化成字符字符串_递归.py
@Author  ：赵文龙 
@Date    ：2023/10/29 23:14 
@功能描述

"""
"""
规定1和A对应， 2和B对应，3和C对应... 26对应Z
"111" ->  AAA KA AK

问题： 给定一个只有数字字符串str, 问可以有多少种转化结果

模型：从左到右模型
从index开始，有两种转化方式
    1、 w1 = index ->  chr
    2、 index+1没有越界，并且int([index][index+1])<27:  w2=[index][index+1] -> chr
        此时 ans = w1 + w2
"""


def way1(num_str):
    if len(num_str) == 0 or num_str is None:
        return 0

    # num_str_list = list(map(int, num_str))

    def process(num_str, index):

        if index == len(num_str):
            return 1

        if int(num_str[index]) == 0:
            return 0

        ans = process(num_str, index + 1)
        if index + 1 < len(num_str) and (int(num_str[index]) * 10 + int(num_str[index + 1]) < 27):
            ans += process(num_str, index + 2)

        return ans

    return process(num_str, 0)


if __name__ == '__main__':
    print(way1("17111"))
