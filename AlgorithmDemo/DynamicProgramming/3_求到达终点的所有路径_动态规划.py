# -*- coding: UTF-8 -*-
"""
@Project ：baseGra 
@File    ：3_求到达终点的所有路径_动态规划.py
@Author  ：赵文龙 
@Date    ：2023/10/25 22:40 
@功能描述

"""
"""
分析：

已知dp的大小，并且dp表的每个位置都可以填入对应的值
N个位置：       N=5
出发点cur：     2
目标点aim：     4
剩余步数K：     6
dp的大小:      range(N+1) range(rest+1) dp[0-6)[0-7)
最后返回       dp[aim][K]



分析计算过程：

   rest     
cur  0   1   2   3   4   5   6
 0   x   x   x   x   x   x   x
 1   0   0   0   1   0   4   0
 2   0   0   1   0   4   0   13
 3   0   1   0   3   0   9   0
 4   1   0   2   0   5   0   14
 5   0   1   0   2   0   5   0


规律：
如果想顺序的填完整dp:
    
    1、首先从 暴力递归的 base case开始：
        if rest == 0:
            return 1 if cur == aim else 0
        结论：rest=0时, 只有aim这个点的值为1, 其他点值都是0， dp[aim][0] = 1, 于是第一列的已经全部填写完整了
    2、当cur在第1行时， dp[1][rest] = dp[2][rest-1] 只和他左下角位置的值相等
        if cur == 1:
            return process(2, rest - 1, aim, N)
    3、当cur在第N行时：dp[N][rest] = dp[N-1][rest-1] 只和他左上角位置的值相等
        if cur == N:
            return process(N - 1, rest - 1, aim, N)
    4、当在cur、rest位置时：dp[cur][rest] = dp[cur-1][rest-1] + dp[cur+1][rest-1]
        return process(cur - 1, rest - 1, aim, N) + process(cur + 1, rest - 1, aim, N)
        
    5、 因此填写顺序是：  0->K  1->N,  循环赋值时就按照这个顺序
    
    6、最终的返回值 dp[start][K]  类似于 process(cur, rest, aim, N)
  
"""


def process3(N, K, start, aim):
    dp = [[0 for x in range(K + 1)] for y in range(0, N + 1)]

    dp[aim][0] = 1
    for rest in range(1, K + 1):
        # 第一行
        dp[1][rest] = dp[2][rest - 1]
        #
        for cur in range(2, N):
            dp[cur][rest] = dp[cur - 1][rest - 1] + dp[cur + 1][rest - 1]
        # 第N行
        dp[N][rest] = dp[N - 1][rest - 1]
    return dp[start][K]


if __name__ == '__main__':
    print(process3(5, 6, 2, 4))
