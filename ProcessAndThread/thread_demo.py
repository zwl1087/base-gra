# -*- coding: utf-8 -*-
"""
@Time ： 2022/12/17 1:35
@Auth ： zhaowenlong
@File ：thread_demo.py
@Software: PyCharm

线程的创建、启动、使用

线程的状态：

新建 --start()--> 就绪 ----> 运行 ----> 结束
新建 --start()--> 就绪 ----> 运行 --sleep()--> 阻塞 --weak up--> 就绪 ----> 运行 ----> 结束

"""
import time
from threading import Thread


def download(sec):
    print("downloading")
    files = ["zhaowenlong.jpg", "huqingling.xlsx", "xiaoxiaonaofu.docx"]
    for file in files:
        print(f"{file} is downloading now")
        time.sleep(sec)
        print(f"{file} had already downloaded ! ")


def listen_music(sec):
    print("listening music")
    music_pool = ["喜羊羊", "大风吹", "葡萄树", "探清水郎"]
    for music in music_pool:
        print(f"{music} is begin playing now ")
        time.sleep(sec)
        print(f"{music} is stop playing")


def edit_office(sec):
    print("edit office file ")
    tasks = ["word", "pdf", "excel", "ppt", "ps", "CAD"]
    for task in tasks:
        print(f"{task} file is editing now")
        time.sleep(sec)
        print(f"{task} file had edited")


if __name__ == '__main__':
    t1 = Thread(target=download, name="t1_thread", args=(1,))
    t2 = Thread(target=listen_music, name="t2_thread", args=(1,))
    t3 = Thread(target=edit_office, name="t3_thread", args=(1,))

    t1.start()
    t2.start()
    t3.start()

    num = 1
    while num <= 2:
        time.sleep(1)
        print(f"-------->  this is main process working {num}")
        num += 1
