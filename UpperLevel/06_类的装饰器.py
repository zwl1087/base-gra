# -*- coding: utf-8 -*-
"""
@Time ： 2023/2/12 23:04
@Auth ： zhaowenlong
@File ：06_类的装饰器.py
@Software: PyCharm
@function:
    定义类的装饰器

"""
from fastapi import Response


def demo(cls):
    print("这是一个类的装饰器")
    return cls


@demo
class Demo:
    def __init__(self):
        print("这是被装饰的类")


d = Demo()


def ResponseDemo(cls):
    user_info = {'name': cls.name, 'age': cls.age, 'sex': cls.sex, 'in_class': cls.in_class}

    res = {
        "code": 0,
        "data": [user_info],
        "msg": "success"
    }
    print(res)
    return cls


@ResponseDemo
class UserResponse:
    def __init__(self, name, age, sex, in_class):
        self._name = name
        self._age = age
        self._sex = sex
        self._in_class = in_class

    @property
    def name(self):
        return self._name

    @property
    def age(self):
        return self._age

    @property
    def sex(self):
        return self._sex

    @property
    def in_class(self):
        return self._in_class


u = UserResponse("zhaowenlong", 20, "M", "class_3")
