# -*- coding: utf-8 -*-
"""
@Time ： 2023/8/19 23:15
@Auth ： zhaowenlong
@File ：06_二叉树_宽度遍历.py
@Software: PyCharm
"""
from AlgorithmDemo.BinaryTree import Nodes
from queue import Queue


def breach_first_traversal(head: Nodes):
    """
    二叉树宽度有限遍历：宽度遍历用队列
    :param head:
    :return:
    """
    print("二叉树宽度有限遍历结果： ")
    q = Queue()
    if head is not None:
        q.put(head)
        while not q.empty():
            head = q.get()
            print(head.data, end='')
            if head.left is not None:
                q.put(head.left)
            if head.right is not None:
                q.put(head.right)
    print()


def is_complete_binary_tree(head: Nodes):
    """
        判断二叉树是否是完全二叉树
    :param head:
    :return:
    """
    q = Queue()
    leaf = False

    if head is not None:
        q.put(head)
        while not q.empty():
            head = q.get()
            if head.right is not None and head.left is None:
                return False

            if leaf and (head.right is not None or head.left is not None):
                return False

            if head.left is not None:
                q.put(head.left)
            if head.right is not None:
                q.put(head.right)
            if head.left is None or head.right is None:
                leaf = True

    return True


if __name__ == '__main__':
    from binarytree import tree

    b = tree(2, True)
    print(b.is_perfect)
    print(b)

    print(is_complete_binary_tree(b))
