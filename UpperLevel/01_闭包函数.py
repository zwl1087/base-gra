# encoding:utf8

import time

"""
【什么是闭包】
闭包： 函数的返回值是一个函数；本质是一个函数。
"""

"""
【如何定义闭包】
1、要有函数的嵌套：内部函数和外部函数
2、内部函数必须使用外部函数的变量
3、外部函数的返回值是内部函数的函数名
"""


def outer_func(a):
    def inner_func(b):
        print("this is inner function")
        return a + b

    return inner_func


"""
【如何使用闭包】
1、调用外部函数，用一个变量名指向内部函数d
2、通过变量调用内部函数，即可完成闭包的调用
"""
f1 = outer_func(10)
f2 = f1(5)
print(f2)

"""
实例：求两点间的坐标距离
"""

"""
内部函数修改外部函数的变量
"""


def outer_func(a):
    def inner_func(b):
        # 内部函数修改外部函数的变量
        nonlocal a
        a += 100
        print("this is inner function")
        return a + b

    return inner_func


f1 = outer_func(10)
f2 = f1(5)
print(f2)

"""
特殊用途：再不修改源代码的情况加添加功能： 计时器，添加答应日志
"""
