# -*- coding: utf-8 -*-
"""
@Time ： 2023/6/22 23:23
@Auth ： zhaowenlong
@File ：01_逻辑运算.py
@Software: PyCharm
"""

'''
and: 逻辑与（&&）
or:  逻辑或（||）
not: 逻辑非（!）




'''

print(1 > 2 and 2 > 3)
print(1 == 1 and 2 == 3)
print(1 == 1 and 2 <= 3)

# 逻辑与
print(True and True)
print(True and False)
print(False and True)
print(False and False)

# 逻辑或
print(True or True)
print(True or False)
print(False or True)
print(False or False)

# not
print(not True)
print(not False)



