# -*- coding: utf-8 -*-
"""
@Time ： 2022/12/18 10:17
@Auth ： zhaowenlong
@File ：thread_share.py
@Software: PyCharm

线程共享资源

"""
from threading import Thread

n = 0


def task_1():
    global n
    for i in range(1000000):
        n += 1
    print("task_1: ", n)


def task_2():
    global n
    for i in range(1000000):
        n += 1
    print("task_2: ", n)


if __name__ == '__main__':
    t1 = Thread(target=task_1)
    t2 = Thread(target=task_2)
    t1.start()
    t1.join()
    t2.start()
    t2.join()

    print("n: ", n)

