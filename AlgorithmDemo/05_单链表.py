"""
单链表：

链表和数组的区别：
数组是内存是连续的
链表内存是不固定的，链表包含：节点和指向

单链表： 单列表的每一个节点都包含两个值： data=节点的值, next=指向;
head 指向第一个节点
tail 指向null
next 指向下一个节点
使用场景：

循环链表： 每一个节点都包含两个值： data=节点的值, next=指向;
head 指向第一个节点
tail 指向第一个节点，也就是head； 因此也是循环的
使用场景：

双向列表： prev=指向上一个节点，data=节点的值, next=指向下一个节点;
第一个节点： prev=null, next=指向下一个节点;
最后一个节点：prev=指向上一个节点, next=null
head： 在第一个节点
tail: 最后一个节点： 最先入栈的节点

双向循环链表：

"""


class SinglyLink:
    """
    实现一个链表：
        1、 从链表头部 插入元素
        2、从链表尾部 插入元素
        3、在节点前插入元素
        4、在节点后插入元素
        5、通过节点值查询节点
        6、通过节点值删除节点
        7、将全部节点的值输出print
        8、实现链表反转

    """

    def __init__(self):
        self.head = None

    def insert_data_to_head(self, data):
        """
        实现从链表的头部插入元素： 将新节点的指针指向head,将head移到新节点上
        :param data: 节点的数据
        :return:
        """
        # 实例化一个新节点： 新节点的数据是data,指向None
        new_node = self.Node(data)
        # 判断是否空链表
        if self.head is None:
            # 将head指向新节点
            self.head = new_node
        else:
            # 不是空节点，就将新节点指向head
            new_node.next = self.head
            # 同时将head移动到新节点上
            self.head = new_node

    def insert_data_to_tail(self, data):
        """
        实现从尾部插入元素： 如果是空链表，相当于是从头部插入元素，如果非空，先找到尾节点，然后将为节点的指针指向新节点
        :param data: 节点的数据
        :return:
        """
        new_node = self.Node(data)
        # 判断是否是空链表
        if self.head is None:
            # 相当于是从头部插入数据
            self.insert_data_to_head(data)
            # 插入数据后不在进行其他操作
            return
        # 如果不是空列表，首先需要找尾节点
        tmp_node = self.head
        # 从头节点开始循环查找
        while tmp_node.next is not None:
            # 如果不是为节点，将head移到下一个节点
            tmp_node = tmp_node.next
        # 找到尾节点时，即tmp_node 就是尾节点，将指针指向新节点
        tmp_node.next = new_node

    def print_all_node(self):
        """
        实现打印链表的所有元素
        :return:
        """
        # 空链表不做任何操作，直接return
        if self.head is None:
            return
        tmp_node = self.head
        # 如果不是空列表，且节点不是非空，则打印节点的data值
        while tmp_node is not None:
            print("tmp_node.data: ", tmp_node.data )
            tmp_node = tmp_node.next

    def del_node_by_value(self, data):
        """
        实现根据节点值在链表中查找节点并删除节点(只删除第一个)： 找到节点前一个节点的 next1, 找到节点下一个节点的 next2,将  next1 指向 next2，此时完成删除
        :param data:
        :return:
        """
        if self.head is None:
            return False
        # 将 head 所指向的第一个节点设置成临时节点： 从临时节点开始遍历，
        tmp_node = self.head
        # 定义临时节点的前驱节点
        tmp_head = None
        # 开始循环遍历：节点不是空的，且节点值不等于data
        while tmp_node is not None and tmp_node.data != data:
            # 前驱节点移动
            tmp_head = tmp_node
            # 临时节点移动到下一个
            tmp_node = tmp_node.next
        # 链表中没有节点值为data时：没有这个值就不删除
        if tmp_node is None:
            return False
        # 判断第一个节点的值是不是data
        if tmp_head is None:
            # 相当于删除头节点
            self.head = self.head.next
        else:
            # 将前驱的节点的指针指向临时节点的后一个节点姐tmp.node.next
            tmp_head.next = tmp_node.next
        return True

    def find_node_by_data(self, data):
        """
        实现通过节点值查找节点
            1、空链表则返回
            2、如果按条件查询没有结果，则返回None
            3、如果有结果，则返回节点
        :param data:
        :return:
        """
        # 空链表则返回
        if self.head is None:
            return
        tmp_node = self.head
        # 按条件遍历链表
        while tmp_node is not None and tmp_node.data != data:
            tmp_node = tmp_node.next
        # 如果按条件查询没有结果，则返回None
        if tmp_node is None:
            return None
        else:
            # 如果有结果，则返回节点
            return tmp_node

    def insert_after_node(self, node, data):
        """
        实现在node节点之后插入新的节点
            1、空链表时，直接返回
            2、直接插入新的节点： new_node.next=node.next; node.next = new_node;
        :param node:
        :param data:
        :return:
        """
        if self.head is None:
            return

        new_node = self.Node(data)
        new_node.next = node.next
        node.next = new_node

    def insert_before_node(self, node, data):
        """
        实现在链表节点前面插入节点：
            空链表时，直接在头部插入
            如果node节点不存在时，直接return
            如果node节点存在，则插入节点： 将node节点的前驱节点指向new_node, 将new_node指向node节点，完成插入
        :param node: 定位节点
        :param data: 节点data
        :return:
        """
        # 如果时空节点，从头部插入
        if self.head is None:
            self.insert_data_to_head(data)
            return
        # 定义一个临时节点： 作为node节点的前驱节点
        tmp_node = self.head
        while tmp_node is not None and tmp_node.next != node:
            tmp_node = tmp_node.next
        # 如果在链表中没有找到node节点，则返回
        if tmp_node is None:
            return
        # 创建一个新的节点
        new_node = self.Node(data)
        # 在node节点前插入新的节点
        new_node.next = node
        tmp_node.next = new_node

    def reverse_link(self):
        """
        实现列表反转：
            1、定义两个临时节点： cur是head所在的节点，pre 表示cur的下一个节点
            2、切换指针： tmp 设为cur的临时节点，将cur的指针指向pre; 【实现切换指针方向】
            3、切换节点： pre 移动到cur; cur移动到tmp
        :return:
        """
        if self.head is None:
            return None
        cur = self.head
        pre = None
        while cur is not None:
            tmp = cur.next
            cur.next = pre
            pre = cur
            cur = tmp

        # 列表反转后, 重置链表的头节点
        self.head = pre
        return self.head

    class Node:
        def __init__(self, data):
            self.data = data
            self.next = None


if __name__ == '__main__':
    link = SinglyLink()
    # map(link.insert_data_to_tail(), [i for i in range(6)])
    for i in range(1, 6):
        link.insert_data_to_tail(i)
    link.print_all_node()
    # print(link.reverse_link())
    print("-------")
    link.reverse_link()
    link.print_all_node()
    print(link)
