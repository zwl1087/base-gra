# -*- coding: utf-8 -*-
"""
@Time ： 2023/6/22 23:40
@Auth ： zhaowenlong
@File ：02_逻辑运算符_短路运算.py
@Software: PyCharm
"""

'''
短路逻辑规则如下：
    表达式从左至右运算，
    若 or 的左侧逻辑值为 True, 则短路 or 后所有的表达式（不管是 and 还是 or），直接输出 or 左侧表达式 。 -->  【短路右侧，直接输出左侧】
    若 or 的左侧逻辑值为 False, 则输出or右侧的表达式，不论其后表达式是真是假，整个表达式结果即为其后表达式的结果表达式从左至右运算，
    若 and 的左侧逻辑值为 False, 则短路其后所有 and 表达式，直到有 or 出现，输出 and 左侧表达式到 or 的左侧，参与接下来的逻辑运算。  --> 【短路and - or 之间的所有的表达式】
    若 and 的左侧逻辑值为 True, 则输出其后的表达式，不论其后表达式是真是假，整个表达式结果即为其后表达式的结果
    若 or 的左侧为 False ，或者 and 的左侧为 True 则不能使用短路逻辑。
注意：
    1、在Python中and的优先级是大于or的，而且and和or都是会返回值的并且不转换为True和False。当not和and及or在一起运算时，优先级为是not>and>or
    2、在Python中，None、任何数值类型中的0、空字符串“”、空元组()、空列表[]、空字典{}都被当作False，
    还有自定义类型，如果实现了 　__ nonzero __ ()　或　__ len __ () 方法且方法返回 0 或False，则其实例也被当作False，其他对象均为True。

'''

# 若 and 的左侧逻辑值为 False, 则短路其后所有 and 表达式，直到有 or 出现，输出 and 左侧表达式到 or 的左侧，参与接下来的逻辑运算。
res = 0 and 1 and 2 and 0 or 1+1
print(res)

print(1 or 0)  # 短路右侧
print(0 or 2)  # 短路左侧
print(0 or [])  # 短路左侧
