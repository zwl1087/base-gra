"""
列表：
线性结构: 连续的、数据类型相同的
"""


class Array(object):
    def __init__(self, cap) -> None:
        # 定义数组
        self.data = [-1] * cap
        # 定义数组的容量
        self.n = cap
        # 记录数组内部数据的个数
        self.count = 0

    def insert(self, location, value):
        """
        在数组中插入数据

        :param location: 插入元素的位置
        :param value: 插入的数据
        :return: Boolean： True插入成功，False插入失败
        """
        # 如果 数组内元素个数已经达到容量，则不能再添加元素
        if self.n == self.count:
            return False
        # 如果 插入元素的位置不时从头开始，或者不是连续的，则不允许插入数据
        if location < 0 or location > self.count:
            return False

        # 将当前位置数据搬到后一个位置，将前一个位置的数据移到当前位置
        for i in range(self.count, location, -1):
            self.data[i] = self.data[i - 1]
        # 在空余的位置将数据插入
        self.data[location] = value
        # 更新数组中元素的个数
        self.count += 1
        return True

    def find(self, location):
        """
        删除指定位置的元素：
        原理： 将对应位置的元素，逐个向前移动一个位置
        :param location:
        :return:
        """
        if location < 0 or location > self.count:
            return False
        return self.data[location]

    def delete(self, location):
        if self.count == 0:
            return
        if location < 0 or location > self.count:
            return False

        for i in range(location+1, self.count):
            self.data[i-1] = self.data[i]
        self.count -= 1

        return True


if __name__ == '__main__':
    array = Array(5)
    print(array)
    array.insert(0, 0)
    array.insert(1, 1)
    assert not array.insert(-1, -1)
    array.insert(2, 2)
    array.insert(3, 3)
    array.insert(4, 4)
    assert not array.insert(5, 5)
    assert array.data == [0, 1, 2, 3, 4]
