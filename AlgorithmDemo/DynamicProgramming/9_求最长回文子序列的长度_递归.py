# -*- coding: UTF-8 -*-
"""
@Project ：baseGra 
@File    ：9_求最长回文子序列的长度_递归.py
@Author  ：赵文龙 
@Date    ：2023/11/1 23:01 
@功能描述

"""

"""
给定一个字符串str, 返回这个字符串的最长回文子序列长度
比如： str = "a12b3c43def2ghi1mk"

最长回文子序列： 123c321  1234321
长度为： 7

解题思路：
    1、 场景分析
        
        构造 str' 是str的逆序列,  逆序列和str的最大子序列的长度就是str的回文子序列的长度
         
    2、 范围分析
        构造递归： f(str, l, r) 返回在 l,r 上的最大回文子序列
        因此整个解就是  return f(str, 0, len(str)-1)

"""


def way1(target_str):
    if target_str is None or len(target_str) == 0:
        return 0

    N = len(target_str)

    def process(tar, l, r):
        # 只剩一个字符
        if l == r:
            return 1
        # 剩下2个字符
        if l == r - 1:
            return 2 if tar[l] == tar[r] else 1

        p1 = process(tar, l, r - 1)
        p2 = process(tar, l + 1, r)
        p3 = process(tar, l + 1, r - 1)
        p4 = process(tar, l + 1, r - 1) + 2 if tar[l] == tar[r] else 0

        return max(p1, p2, p3, p4)

    return process(target_str, 0, N - 1)


def way2(target_str):
    N = len(target_str)
    dp = [[1 if i == j else 0 if i > j else '#' for i in range(N)] for j in range(N)]
    # print(dp)

    for l in range(N - 1):
        dp[l][l + 1] = 2 if target_str[l] == target_str[l + 1] else 1

    for l in range(N - 3, -1, -1):
        for r in range(l + 2, N):
            p1 = dp[l][r-1]
            p2 = dp[l+1][r]
            p3 = dp[l+1][r-1]
            p4 = dp[l+1][r-1] + 2 if target_str[l] == target_str[r] else 0
            dp[l][r] = max(p1, p2, p3, p4)

    for item in dp:
        print(item)

    return dp[0][N - 1]


if __name__ == '__main__':
    print(way1("a12b3c43def2ghi1mk"))
    print(way2("a12b3c43def2ghi1mk"))
