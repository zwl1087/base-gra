# -*- coding: utf-8 -*-
"""
@Time ： 2023/7/16 23:10
@Auth ： zhaowenlong
@File ：05_双链表.py
@Software: PyCharm
"""


class DoubleLink:

    def __init__(self):
        self.head = None
        # self.tail = None

    def insert_to_head(self, data):
        """
            从双列表的头部插入数据
        :param data:
        :return:
        """
        new_node = self.Node(data)
        # 头节点为空, 也即在空列表中插入节点
        if self.head is None:
            self.head = new_node

        # 新节点的next指向当前头
        new_node.next = self.head
        # 当前头的prev 指向新节点
        self.head.prev = new_node
        # 将头节点移动到 新节点上
        self.head = new_node

    def insert_to_tail(self, data):
        """
          从双链表的尾部插入数据
        :param data:
        :return:
        """
        new_node = self.Node(data)
        if self.head is None:
            self.head = new_node

        cur = self.head
        while cur.next:
            cur = cur.next

        cur.next = new_node
        new_node.prev = cur

    class Node:
        """ 双链表节点数据结构 """
        def __init__(self, data):
            self.data = data
            self.next = None   # 向后指针
            self.prev = None   # 向前指针
